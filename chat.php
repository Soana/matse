<?php
	require_once("databaseConfig.php");
	require_once("src/Parse.php");
	session_start(); //Start the session holding the logininformation (possibly)
	$userid = $_SESSION["userid"];
	if(isset($_SESSION) && isset($userid) && !empty($userid)) //If the user is logged in
	{
		$dba = new mysqli($GLOBALS["databaseConfig"]["URL"],$GLOBALS["databaseConfig"]["User"],$GLOBALS["databaseConfig"]["Password"]); //And setup the databaseconnection
		$dba -> select_db($GLOBALS["databaseConfig"]["Database"]); //Select the database to read from
		echo($dba->error); //Echo any errors belonging to the databasesetup
		$action = $_GET["ac"];
		if($action == "recv")
		{
			$id = $_GET["id"];
			if(isset($id))
			{
				if($id == 0) //Joined newly and demands a whole poll
				{
					$query = $dba -> prepare("SELECT chat.ID, chat.Message, chat.Date, user.Name FROM Chat chat LEFT JOIN Users user ON chat.User = user.ID ORDER BY chat.ID DESC LIMIT 50");
					$query -> execute();
					$query -> bind_result($id, $msg, $date, $name);
					while($query -> fetch())
					{
						echo($id.",".str_replace(",", "&comma;", $msg).",".$date.",".$name."<next>");
					}
					$query -> close();
				}
				else //Just needs the deltas
				{
					$query = $dba -> prepare("SELECT chat.ID, chat.Message, chat.Date, user.Name FROM Chat chat LEFT JOIN Users user ON chat.User = user.ID WHERE chat.ID > ? ORDER BY chat.ID ASC");
					$query -> bind_param("i", $id);
					$query -> execute();
					$query -> bind_result($id, $msg, $date, $name);
					while($query -> fetch())
					{
						echo($id.",".str_replace(",", "&comma;", $msg).",".$date.",".$name."<next>");
					}
					$query -> close();
				}
			}
			else echo("0");
		}
		else if($action == "send")
		{
			$text = $_POST["msg"];
			$query = $dba -> prepare("INSERT INTO Chat (User, Date, Message) VALUES (?, ?, ?)");
			$now = time();
			$text = parse($text);
			echo($text);
			$query -> bind_param("iis", $userid, $now, $text);
			$query -> execute();
			$id = $dba -> insert_id;
			$query -> close();
			if($id % 200 == 0)
			{
				$start = $id - 200;
				$query = $dba->prepare("DELETE FROM Chat WHERE ID < ?");
				$query -> bind_param("i", $start);
				$query -> execute();
				$query -> close();
			}
			echo("1");
		}
		else if($action == "login")
		{
			$query = $dba -> prepare("UPDATE Users SET Online = 1 WHERE ID = ?");
			$query -> bind_param("i", $userid);
			$query -> execute();
			$query -> close();
		}
		else if($action == "logout")
		{
			$query = $dba -> prepare("UPDATE Users SET Online = 0 WHERE ID = ?");
			$query -> bind_param("i", $userid);
			$query -> execute();
			$query -> close();
		}
		else if($action == "users")
		{
			$query = $dba -> prepare("SELECT Name FROM Users WHERE Online = 1");
			$query -> execute();
			$query -> bind_result($name);
			while($query -> fetch())
			{
				echo($name."\n");
			}
			$query -> close();
		}
		else echo("0");
	}
	else echo("0");
	
?>