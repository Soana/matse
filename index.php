<?php
	require_once("databaseConfig.php");
	$time = microtime(true); //Start of timemeasurement
	require_once("src/Matse.php");
	$matse = new Matse(); //Create the matseinstance
	foreach($_POST as $key => $val)
	{
		$_POST[$key] = str_replace("<", "&lt;", str_replace(">", "&gt;", $val));
	}
	if(empty($_GET["silent"])) $_GET["silent"] = 0;
	if(!$_GET["silent"])
	{
		?>
		
			<!DOCTYPE HTML>
			<html>
				<head>
					<link rel="shortcut icon" href="favicon.png" type="image/png" />
					<link rel="icon" href="favicon.png" type="image/png" />
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
					<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
					<script type="text/javascript" src="js/sh/shCore.js"></script>
					<script type="text/javascript" src="js/sh/shBrushJava.js"></script>
					<script type="text/javascript" src="js/sh/shBrushCpp.js"></script>
					<script type="text/javascript" src="js/sh/shBrushCSharp.js"></script>
					<script type="text/javascript" src="js/sh/shBrushJScript.js"></script>
					<script type="text/javascript" src="js/sh/shBrushPhp.js"></script>
					<script type="text/javascript" src="js/sh/shBrushPython.js"></script>
					<script type="text/javascript" src="js/sh/shBrushSql.js"></script>
					<script type="text/javascript" src="js/sh/shBrushPlain.js"></script>
					<script type="text/javascript" src="js/toggle.js"></script>
					<script type="text/javascript" src="js/cookies.js"></script>
					<link rel="stylesheet" type="text/css" href="style/style.css" />
					<link rel="stylesheet" type="text/css" href="style/shCoreDefault.css" />
					<title>Matse</title>
				</head>
				<body>
					<div class="wrapper">
		<?php
	}
	$matse->printHTML(); //Render the whole page
	if(!$_GET["silent"])
	{
		?>
						<div class="footer">
							Generated page in  <?php echo((microtime(true)-$time)/1000); ?>ms | <?php echo($matse->querys); ?> querys opened | An OpenSource Project, visit <a style="text-decoration: underline;" href="http://cronosx.de:5990/MATSE">cronosx.de:5990/MATSE</a> | <a style="text-decoration: underline;" href="?action=impressum">Impressum</a>
						</div>
					</div>
				</body>
			</html>
		<?php
	}