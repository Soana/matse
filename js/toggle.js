var elems = {};

$(function()
{
	var cookie = getCookie("toggled");
	if(cookie != null && cookie != "")
	{
		var arr = dec(cookie);
		$.each(arr, function(key, val) 
		{
			if(val == "true")
			{
				$('#'+ key).css('height', 'auto');
				elems[key] = true;
			}
		});
	}
});

function toggle(id)
{
	var elem = $('#'+id);
	if(elems[id])
	{
		elems[id] = false;
		setCookie("toggled", enc(elems), 100000); 
		elem.animate(
		{
			height: '35px'
		}, 80);
	}
	else
	{
		elems[id] = true;
		setCookie("toggled", enc(elems), 100000); 
		var height1 = elem.height();
		elem.css('height', 'auto');
		var height2 = elem.height();
		elem.css('height', height1);
		elem.animate(
		{
			height: height2
		}, 80);
	}
	console.log(enc(elems));
};

function enc(array)
{
	var res = "";
	$.each(array, function(key, val) 
	{
		var str = key + ":" + val;
		res += str + ",";
	});
	return res;
};

function dec(str)
{
	var array = {};
	var keysets = str.split(",");
	$.each(keysets, function(idx, val)
	{
		var pair = val.split(":");
		array[pair[0]]=pair[1];
	});
	return array;
};

(function($, undefined)
{
	$.fn.getCursorPosition = function()
	{
		var el = $(this).get(0);
		var pos = 0;
		if('selectionStart' in el)
		{
			pos = el.selectionStart;
		}
		else if('selection' in document)
		{
			el.focus();
			var Sel = document.selection.createRange();
			var SelLength = document.selection.createRange().text.length;
			Sel.moveStart('character', -el.value.length);
			pos = Sel.text.length - SelLength;
		}
		return pos;
	}
})(jQuery);

new function($) {
	$.fn.setCursorPosition = function(pos) 
	{
		if ($(this).get(0).setSelectionRange) 
		{
			$(this).get(0).setSelectionRange(pos, pos);
		}
		else 
			if ($(this).get(0).createTextRange) 
			{
				var range = $(this).get(0).createTextRange();
				range.collapse(true);
				range.moveEnd('character', pos);
				range.moveStart('character', pos);
				range.select();
			}
	}
}(jQuery);