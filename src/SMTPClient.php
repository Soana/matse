<?php
class SMTPClient
{
	private $smtpServer; //Server to connect to
	private $smtpUser; //User to athenticate with
	private $smtpPass; //Password to authenticate with
	private $from; //Origin of the mail
	private $smtpPort; //Port of the server (usually 25)
	private $response; //Responsestring of the server
	/*
	 * Create a new smtpClient instance and store its configuration
	 */
	public function __construct($smtpServer, $smtpPort, $smtpUser, $smtpPass, $from)
	{
		$this->smtpServer = $smtpServer; //Store all data
		$this->smtpUser = base64_encode ($smtpUser);
		$this->smtpPass = base64_encode ($smtpPass);
		$this->from = $from;
		if(empty($smtpPort))
			$this->smtpPort = 25;
		else
			$this->smtpPort = $smtpPort;
	}
	
	/*
	 * Connect to the specified server
	 */
	function connect()
	{
		$ok = $this -> socket = fsockopen ($this->smtpServer, $this->smtpPort); //Try to open socket
		if($ok) //If socket is connected
		{
			fputs($this -> socket, "EHLO ".$_SERVER["HTTP_HOST"]."\r\n"); //Greet the server
			$this -> response .= fgets ( $this -> socket, 1024 ) + "\n";
			return true;
		} 
		else return false;
	}
	
	/*
	 * Disconnect and close the socket
	 */
	function disconnect()
	{
		fputs ($this -> socket, "QUIT\r\n");//Say goodbye
		fclose($this -> socket);//And close the socket
	}
	
	/*
	 * Login with the specified data
	 */
	function login()
	{
		fputs($this -> socket, "auth login\r\n"); //Start authentication
		$this -> response .= fgets($this -> socket,1024); //Send logindata
		fputs($this -> socket, $this->smtpUser."\r\n");
		$this -> response .= fgets($this -> socket,1024);
		fputs($this -> socket, $this->smtpPass."\r\n");
		$this -> response .= fgets($this -> socket,256);
	}

	/*
	 * Send the specified mail
	 */
	function sendMail($to, $subject, $body)
	{
		$this -> response = "";
		if($this -> connect())//Connect
		{
			$this -> login(); //Login
			fputs ($this -> socket, "MAIL FROM: <".$this->from.">\r\n"); //Send mail
			$this -> response .= fgets ( $this -> socket, 1024 );
			fputs ($this -> socket, "RCPT TO: ".$to."\r\n");
			$this -> response .= fgets ($this -> socket, 1024);
			fputs($this -> socket, "DATA\r\n");
			$this -> response .= fgets( $this -> socket,1024 );
			fputs($this -> socket, 
				"To: <".$to.">\r\n".
				"From: ".$this->from."\r\n".
				"Subject:".$subject."\r\n".
				"Content-Type:text/plain; charset=UTF-8\r\n".
				$body."\r\n.\r\n");
			$this -> response .= fgets($this -> socket,256);
		}
	}
	
	/*
	 * For debug purposes, get response from server
	 */
	function getResponse()
	{
		return $this -> response; //Get responsestring of last sent mail
	}
}
?>