<?php
	/*
	 * Prototype for all Contents
	 * Contains a pointer to the main class of the program
	 * And defines the default constructor
	 */
	class Content
	{
		protected $matse;//Pointer to the main class of the program
		public function __construct($matse) //Default constructor
		{
			$this->matse = $matse;//Save pointer
		}
	}
?>