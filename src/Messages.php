<?php 
	function displayError($string)
	{
		?>
			<div class="error">
				<h1>Warnung</h1>
				<?php echo($string); ?>
			</div>
		<?php 
	}
	
	function displayWarning($string)
	{
		?>
			<div class="error">
				<h1>Warnung</h1>
				<?php echo($string); ?>
			</div>
		<?php 
	}
	
	function displaySuccess($string)
	{
		?>
			<div class="success">
				<h1>Erfolgreich</h1>
				<?php echo($string); ?>
			</div>
		<?php
	}

?>