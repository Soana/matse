<?php 
	/*
	 * Will display all users currently registered to this server
	 */
	class Contentuserlist extends Content
	{
		public function printH1()
		{
			echo("Liste aller Benutzer");
		}
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			?>
				<table>
					<tr class="head">
						<td>Name</td>
						<?php  //If this user (currently logged in) has the privileg to manage other users privileges, display the tableheader
							if($this->matse->user->hasPrivileg("ManagePrivileges"))
							{
								?>
									<td>Privilegien</td>
								<?php
							}
							if($this->matse->user->hasPrivileg("DeleteUser"))
							{
								?>
									<td>Löschen</td>
								<?php
							}
							if($this->matse->user->hasPrivileg("RenameUser"))
							{
								?>
									<td>Umbenennen</td>
								<?php
							}
						?>
					</tr>
					<?php 
						$size = 20;
						if(isset($_GET["num"]))
						{
							$page = $_GET["num"];
						}
						else 
							$page = 1;
						$upper = $page * $size - $size;
						$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM Users");
						$mysqli -> execute();
						$mysqli -> store_result();
						$amnt = $mysqli -> num_rows;
						$mysqli -> close();
						$pages = ceil(($amnt / $size + 0.0));
						$query = $this->matse->db()->prepare("SELECT ID, Name FROM Users LIMIT ?, ?"); //Get all users from the database
						$query -> bind_param("ii", $upper, $size);
						$query->execute();
						$query->bind_result($id, $name);
						while($query->fetch()) //And generate a table with each row a dataset
						{
							?>
								<tr>
									<td><a href="?action=viewUser&id=<?php echo($id); ?>"><?php echo($name); ?></a></td>
									<?php //If this user (currently logged in) has the privileg to manage other users privileges, display him the link
										if($this->matse->user->hasPrivileg("ManagePrivileges"))
										{
											?>
												<td><a href="?action=privileges&user=<?php echo($id); ?>">Verwalten</a></td>
											<?php
										}
										if($this -> matse -> user -> hasPrivileg("DeleteUser"))
										{
											?>
												<td><a href="?action=viewUser&id=<?php echo($id); ?>&delete=1">Löschen</a></td>
											<?php
										}
										if($this -> matse -> user -> hasPrivileg("RenameUser"))
										{
											?>
												<td><form action="?action=viewUser&id=<?php echo($id); ?>&rename=true" method="post">
													<input type="text" name="name" /><input type="submit" value="OK"/>
												</form></td>
											<?php
										}
									?>
								</tr>
							<?php
						}
						$query -> close();
					?>
				</table>
				<div class="chooser">
					<?php 
						for($i = 1; $i <= $pages; $i++)
						{
							if($i == $page)
							{
								?>
									<a class="selected" href="?action=userlist&num=<?php echo($i);?>"><?php echo($i); ?></a>
								<?php
							}
							else
							{
								?>
									<a class="unselected" href="?action=userlist&num=<?php echo($i);?>"><?php echo($i); ?></a>
								<?php
							}
						}
					?>
				</div>
			<?php
		}
	}
?>