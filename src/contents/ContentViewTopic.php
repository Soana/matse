<?php 

	class ContentViewTopic extends Content
	{
		public function printH1()
		{
			echo($this->getTopic());
		}
		
		private function getTopic()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT Topic FROM Questions WHERE ID = ?");
			$mysqli -> bind_param("i", $_GET["id"]);
			$mysqli -> execute();
			$mysqli -> bind_result($topic);
			$mysqli -> fetch();
			$mysqli -> close();
			return $topic;
		}
		
		private function vote()
		{
			if($this -> matse -> user -> loggedIn)
			{
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Reputations WHERE Voter = ? AND Item = ? AND ItemType = 3");
				$mysqli -> bind_param("ii", $this -> matse -> user -> userID, $_GET["voteAnswer"]);
				$mysqli -> execute();
				$mysqli -> close();
				if($_GET["score"] == "1") $score = 1;
				else $score = -1;
				$mysqli = $this -> matse -> db() -> prepare("SELECT a.User, t.Subject FROM Answers a LEFT JOIN Questions t ON a.Topic = t.ID WHERE a.ID = ?");
				$mysqli -> bind_param("i", $_GET["voteAnswer"]);
				$mysqli -> execute();
				$mysqli -> bind_result($user, $subject);
				$mysqli -> fetch();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("INSERT INTO Reputations (User, Voter, Item, ItemType, Subject, Score) VALUES (?, ?, ?, 3, ?, ?)");
				$mysqli -> bind_param("iiiii", $user, $this -> matse -> user -> userID, $_GET["voteAnswer"], $subject, $score);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Lösung wurde erfolgreich mit $score bewertet.");
			}
			else
			{
				displayError("Sie müssen angemeldet sein, um dies zu tun."); 
			}
		}
		
		private function writeAnswer($tid)
		{
			$topic = $this -> getTopic();
			$usern = $this -> matse -> getUsername($this -> matse-> user-> userID);
			$text = parse($_POST["content"]);
			$mysqli = $this -> matse -> db() -> prepare("INSERT INTO Answers (Topic, Date, User, Message) VALUES (?, ?, ?, ?)");
			$now = time();
			$mysqli -> bind_param("iiis", $tid, $now, $this -> matse-> user-> userID, $text);
			$mysqli -> execute();
			$mysqli -> close();
			displaySuccess("Vielen Dank! Deine Antwort wurde erfolgreich gespeichert.");
			$mysqli = $this -> matse -> db() -> prepare("SELECT u.Mail FROM ObservedTopics o LEFT JOIN Users u ON o.User = u.ID WHERE o.Topic = ?");
			$mysqli -> bind_param("i", $tid);
			$mysqli -> execute();
			$mysqli -> bind_result($mail);
			while($mysqli -> fetch())
			{
				$this -> matse -> smtpClient -> sendMail($mail, "Aktivität im Topic \"".$topic."\"", 
						"Hallo,\r\n\r\n".
						"Sie erhalten diese Mail, weil Sie ausgewählt haben, den Topic \"".$topic."\" zu beobachten.\r\n".
						"Der Benutzer $usern hat in dem entsprechenden Topic eine Antwort gepostet.\r\n".
						"Besuchen Sie den Topic, um die Antwort zu lesen.\r\n\r\n".
						"Ihre Matse-Börese");
			}
			$mysqli -> close();
		}

		private function closeTopic($tid)
		{
			if($userID == $this -> matse -> user -> userID || $this -> matse -> user -> hasPrivileg("CloseQuestions"))
			{
				$mysqli = $this -> matse -> db() -> prepare("UPDATE Questions SET Resolved = TRUE WHERE ID = ?");
				$mysqli -> bind_param("i", $tid);
				$mysqli -> execute();
				$mysqli -> close();
				$resolved = true;
				displaySuccess("Frage wurde erfolgreich als beantwortet markiert.");
			}
			else
			{
				displayError("Sie haben nicht die Berechtigung, diese Frage zu schliessen.");
			}
		}
		
		private function deleteAnswer()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT User FROM Answers WHERE ID = ?");
			$mysqli -> bind_param("i", $_GET["deleteAnswer"]);
			$mysqli -> execute();
			$mysqli -> bind_result($userid);
			$mysqli -> fetch();
			$mysqli -> close();
			if($this -> matse -> user -> userID == $userid || $this -> matse -> user ->hasPrivileg("DeleteAnswers"))
			{
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Answers WHERE ID = ?");
				$mysqli -> bind_param("i", $_GET["deleteAnswer"]);
				$mysqli -> execute();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Reputations WHERE ItemType = 3 AND Item = ?");
				$mysqli -> bind_param("i",$_GET["deleteAnswer"]);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Erfolgreich gelöscht");
			}
			else displayError("Sie haben nicht die Berechtigung, dies zu tun");
		}
		
		private function observeTopic($id)
		{
			$mysqli = $this -> matse -> db() -> prepare("DELETE FROM ObservedTopics WHERE User = ? AND Topic = ?");
			$mysqli -> bind_param("ii", $this -> matse->user->userID, $id);
			$mysqli -> execute();
			$mysqli -> close();
			if(!empty($_POST["observe"]) && $_POST["observe"] == "true")
			{
				$mysqli = $this -> matse -> db() -> prepare("INSERT INTO ObservedTopics(User, Topic) VALUES(?, ?)");
				$mysqli -> bind_param("ii", $this -> matse->user->userID, $id);
				$mysqli -> execute();
				$mysqli -> close();
				$topic =$this ->  getTopic();
				$this -> matse -> smtpClient -> sendMail($this -> matse -> user->mail, "Beobachte Topic \"".$topic."\"", 
						"Hallo,\r\n\r\n".
						"Sie erhalten diese Mail, weil Sie ausgewählt haben, den Topic \"".$topic."\" zu beobachten.\r\n".
						"Sie werden zukünftig bei jedem neuen Post in diesem Topic eine E-mail-Benachrichtigung erhalten.\r\n".
						"Bitte besuchen Sie den Topic, um diese Benachrichtigung wieder zu deaktivieren.\r\n\r\n".
						"Ihre Matse-Börse");
				displaySuccess("Sie beobachten diesen Topic jetzt. Eine Bestätigungsmail wurde an Sie gesendet");
			}
			else
			{
				displaySuccess("Sie beobachten diesen Topic nicht mehr");
			}
		}
		
		/*
		 * Render page
		*/
		public function printHTML()
		{
			if(!$this ->matse->user->loggedIn)
			{
				displayWarning("Das Forum steht nur registrierten Benutzern zur Verfügung, Sie können daher nicht auf Fragen antworten.");
			}
			$fehler = false;
			if(isset($_GET["id"]))
			{
				$id = $_GET["id"];
				if(empty($id) || $id == 0)
					$fehler = true;
				else
				{
					if(isset($_GET["voteAnswer"]) && isset($_GET["score"])) $this -> vote();
					if(isset($_GET["deleteAnswer"])) $this -> deleteAnswer();
					if(isset($_GET["observe"])) $this -> observeTopic($id);
					$tid = $_GET["id"];
					if($this ->matse->user->loggedIn && isset($_POST["content"]))$this -> writeAnswer($tid);
					$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM ObservedTopics WHERE User = ? AND Topic = ?");
					$mysqli -> bind_param("ii", $this -> matse -> user -> userID, $id);
					$mysqli -> execute();
					if($mysqli -> fetch())
						$observed = "checked";
					else 
						$observed = "";
					$mysqli -> close();
					$mysqli = $this -> matse -> db() -> prepare("SELECT q.Date, q.Topic, q.User, q.Message, q.Subject, q.Resolved, u.Name, s.Name FROM Questions q Left JOIN Subjects s ON q.Subject = s.ID LEFT JOIN Users u ON u.ID = q.User WHERE q.ID = ?");
					$mysqli -> bind_param("i", $id);
					$mysqli -> execute();
					$mysqli -> bind_result($dateRaw, $topic, $userID, $txt, $subjectID, $resolved, $userName, $subjectName);
					if($mysqli -> fetch())
					{
						$mysqli -> close();
						if(isset($_GET["closeTopic"]) && $_GET["closeTopic"] == "true") $this -> closeTopic($tid);
						$date = date("d.m.Y H:i", $dateRaw);
						if(empty($subjectName)) $subjectName = "kein Fach";
						if(empty($userName)) $userName = "Kein Benutzer";
						?>
							<p>Die Frage wurde <?php echo($date); ?> von <?php echo($userName." "); ?> gestellt.</p>
							<p>Die Frage bezieht sich auf <?php echo($subjectName); ?>.</p>
							<?php
								if($this ->matse->user->loggedIn)
								{
									?>
										<form id="observe" action="?action=viewTopic&id=<?php echo($id);?>&observe=true" method="post">
											<p><input value="true" type="checkbox" name="observe" onclick="javascript: $('#observe').submit();" <?php echo($observed); ?>/> Diesen Topic beobachten (E-Mail Benachrichtigung)</p>
										</form>
									<?php
								}
								if(!$resolved)
								{
									?>
										<p>Diese Frage ist derzeit noch <span style="color: blue; font-style: normal;">Offen</span>. 
										<?php 
											if($userID == $this -> matse -> user -> userID || $this -> matse -> user -> hasPrivileg("CloseQuestions"))
											{
												?>
													Klicken Sie <a href="?action=viewTopic&id=<?php echo($tid); ?>&closeTopic=true">hier</a>, um die Frage zu schliessen.
												<?php
											} 
										
										?>
										</p>
									<?php
								}	
								else
								{
									?>
										<p>Diese Frage wurde bereits beantwortet. </p>
									<?php
								}
							?>
							<div class="text">
								<?php echo($txt);?>
							</div>
							<br>
						<?php
					}
					else 
					{
						?>
							<div class="error">
								<h1>Fehler</h1>
								Datenbankfehler oder ungültige ID. 
							</div>
						<?php 
						$mysqli -> close();
					}
				}
				$mysqli = $this -> matse -> db() -> prepare("SELECT a.ID, a.Date, a.Message, u.Name, u.ID, SUM(r.Score) FROM Answers a LEFT JOIN Users u ON a.User = u.ID LEFT JOIN Reputations r ON r.Item = a.ID AND r.ItemType = 3 WHERE Topic = ? GROUP BY a.ID ORDER BY Date DESC");
				$mysqli -> bind_param("i", $tid);
				$mysqli -> execute();
				$mysqli -> bind_result($id, $dateRaw, $txt, $userName, $userid, $score);
				while($mysqli -> fetch())
				{
					if(empty($score)) $score = 0;
					$date = date("d.m.Y H:i", $dateRaw);
					if(empty($userName)) $userName = "Kein Benutzer";
					if($score <= 0) $scorecolor = "blue";
					else $scorecolor = "green";
					?>
						<div class="homework opened" id="answer_<?php echo($tid);?>">
							<div class="info">
								<?php echo($date); ?> von <?php echo($userName." "); ?>
							</div>
							<div class="text">
								<?php echo($txt);?>
							</div>
							<div class="smallBar">
								<?php 
									if($this -> matse -> user -> loggedIn)
									{
										?>
											<a style="float: right;" href="?action=viewTopic&id=<?php echo($tid); ?>&voteAnswer=<?php echo($id); ?>&score=0">-1</a>
											<a style="float: right;" href="?action=viewTopic&id=<?php echo($tid); ?>&voteAnswer=<?php echo($id); ?>&score=1">+1</a>
										<?php 
									}
									if($this -> matse -> user -> userID == $userid || $this -> matse -> user ->hasPrivileg("DeleteAnswers"))
									{
										?>
											<a href="?action=viewTopic&id=<?php echo($tid); ?>&deleteAnswer=<?php echo($id); ?>">Löschen</a>
										<?php
									}	
								?>
								Reputation: <span style="color: <?php echo($scorecolor); ?>"><?php echo($score); ?></span>
							</div>
						</div>
					<?php
				}
				$mysqli -> close();
				if($this ->matse->user->loggedIn)
				{
					?>
						<div class="homework" id="createAnswer">
							<a href="javascript: toggle('createAnswer')"><h2>Antwort verfassen</h2></a>
							<form action="?action=viewTopic&id=<?php echo($tid); ?>" method="post" style="margin-left: 15px; margin-bottom: 15px">
								<label>Text</label>
								<textarea id="msg" name="content" style="width: 600px; height: 400px">Befehle:&#10;[latex] Latex-Code Hier [/latex]&#10;[code=lang] Sourcecode hier [/code]&#10;Verfügbare Sprachen: c++, java, js, python, php, sql, c#</textarea>
								<label>Antworten</label>
								<input type="submit" value="OK" />
							</form>
						</div>
					<?php 
				}
				?>
					<script type="text/javascript">
						MathJax.Hub.Queue(["Typeset",MathJax.Hub,"messages"]);
						SyntaxHighlighter.highlight();
						var init = false;
						$('#msg').click(function()
						{
							if(!init)
							{
								$('#msg').css("color", "black");
								$('#msg').css("font-style", "normal");
								$('#msg').val("");
								init = true;
							}
						});
					</script>
				<?php
				if($this -> matse -> user -> hasPrivileg("DeleteQuestions"))
				{
					?>
						<p>Sie können die Frage <a href="?action=forum&deleteTopic=<?php echo($tid); ?>">hier</a> löschen</p>
					<?php
				}
			}
			else
				$fehler = true;
			if($fehler)
			{
				displayError("Ein erwarteter Parameter wurde nicht übergeben, die Seite kann nicht angezeigt werden. ");
			}
		}
	}

?>
