<?php 
	/*
	 * Should be displayed if an invalid Content is loaded
	 */
	class Content404
	{
		
		public function printH1()
		{
			echo("Fehler - 404");
		}
		public function printHTML()
		{
			?>
				<p>Die angeforderte Seite existiert leider nicht, bitte rufen Sie eine andere Seite auf!</p>
			<?php
		}
	}

?>