<?php
	/*
	 * Form to create a new homework
	 */
	class ContentHomeworkCreate extends Content
	{

		public function printH1()
		{
			echo("Hausaufgabe anlegen");
		}
		
		private function create()
		{
			$startDate = mktime(0,0,0,$_POST["re_mnt"], $_POST["re_day"], $_POST["re_yr"]); //Calculate unixtimestamp form start- and endtime
			$endDate = mktime(23,59,59,$_POST["re_mnt"], $_POST["re_day"], $_POST["re_yr"]);
			$query = $this->matse->db()->prepare("INSERT INTO Homework (Subject, Name, Date, Due, Location, File) VALUES(?, ?, ?, ?, ?, ?)"); //Insert the data into the database
			$query -> bind_param("isiiis", $_GET["subject"], $_POST["name"], $startDate, $endDate, $_POST["location"], $_POST["file"]);
			$query->execute();
			$query -> close();
			displaySuccess("Die Hausaufgabe ".$_POST["name"]." wurde erfolgreich im Fach <a href=\"?action=subjectView&subject=".$_GET["subject"]."\">".$this->matse->getSubjectName($_GET["subject"])."</a> angelegt.");
		}
		
		/*
		 * Render page
		*/
		public function printHTML()
		{
			if(!$this->matse->user->hasPrivileg("CreateHomework")) //If the user does not have the privileg to enter this page:
			{
				?>
					<div class="error">
						<h1>Fehler</h1>
						
					</div>
				<?php 
				displayError("Unberechtigter Zugriff!");
				return; //Stop rendering the page or execute anything with his posted data and exit
			}
			if(isset($_POST["name"])) //If this is set, the user is sending us a filled form
			{
				$this -> create();
			}
			$year = date("Y"); //Calculate current time to write into the form
			$month = date("m");
			$day = date("d");
			?>
				<form action="#" method="POST">
					<p>
						<label>Name</label>
						<input name="name" type="text">
					</p>
					<p>
						<label>Standort</label>
						<input name="location" id="location" type="radio" value="0" checked="checked"/>Aachen<br>
						<input name="location" id="location" type="radio" value="1"/>Jülich<br>
						<input name="location" id="location" type="radio" value="2"/>Köln<br>
					</p>
					<p>
						<label>Link zum Übungsblatt</label>
						<input name="file" id="file" type="text"/>
					</p>
					<p>
						<label>Erhalten</label>
						<input name="re_day" type="text" id="re_day" style="width: 2em" value="<?php echo($day);?>">.
						<input name="re_mnt" type="text" id="re_mnt" style="margin-left:0; width: 2em" value="<?php echo($month);?>">.
						<input name="re_yr" type="text" id="re_yr" style="margin-left:0; width: 4em" value="<?php echo($year);?>">
						<span id="reInfo" class="info"></span>
					</p>
					<p>
						<label>Abgabe</label>
						<input name="to_day" type="text" id="to_day" style="width: 2em" value="<?php echo($day);?>">.
						<input name="to_mnt" type="text" id="to_mnt" style="margin-left:0; width: 2em" value="<?php echo($month);?>">.
						<input name="to_yr" type="text" id="to_yr" style="margin-left:0; width: 4em" value="<?php echo($year);?>">
						<span id="toInfo" class="info"></span>
					</p>
					<p>
						<label>Speichern</label>
						<input id="submit" type="submit" value="Okay"/>
						<span id="okayInfo" class="info"></span>
					</p>
				</form>
				<script type="text/javascript">
					var reDay = $('#re_day');
					var reMnt = $('#re_mnt');
					var reYr = $('#re_yr');
					var reInfo = $('#reInfo');
					var toDay = $('#to_day');
					var toMnt = $('#to_mnt');
					var toYr = $('#to_yr');
					var toInfo = $('#toInfo');
					var submit = $('#submit');
					submit.check = function()
					{
						if(reDay.okay && reMnt.okay && reYr.okay && toDay.okay && toMnt.okay && toYr.okay)
						{
							submit.removeAttr("disabled");
							submitInfo.text("Okay");
							submitInfo.addClass("okay");
							submitInfo.removeClass("bad");
						}
						else
						{
							submit.attr("disabled", "disabled");
							submitInfo.text("Bitte alles nochmals prüfen");
							submitInfo.removeClass("okay");
							submitInfo.addClass("bad");
						}	
					};
					var submitInfo = $('#okayInfo');
					reDay.keyup(function()
					{
						reDay.okay = !isNaN(reDay.val());
						check(reInfo, reDay, reMnt, reYr);
					});
					reMnt.keyup(function()
					{
						reMnt.okay = !isNaN(reMnt.val());
						check(reInfo, reDay, reMnt, reYr);
					});
					reYr.keyup(function()
					{
						reYr.okay = !isNaN(reYr.val());
						check(reInfo, reDay, reMnt, reYr);
					});
					toDay.keyup(function()
					{
						toDay.okay = !isNaN(toDay.val());
						check(toInfo, toDay, toMnt, toYr);
					});
					toMnt.keyup(function()
					{
						toMnt.okay = !isNaN(toMnt.val());
						check(toInfo, toDay, toMnt, toYr);
					});
					toYr.keyup(function()
					{
						toYr.okay = !isNaN(toYr.val());
						check(toInfo, toDay, toMnt, toYr);
					});

					function check(info, day, mnt, yr)
					{
						if(day.okay && mnt.okay && yr.okay)
						{
							info.text("Okay");
							info.removeClass("bad");
							info.addClass("okay");
						}
						else
						{
							info.text("Das ist kein Datum (TT.MM.YYYY)");
							info.addClass("bad");
							info.removeClass("okay");
						}
						submit.check();
					}
					reDay.keyup();
					reMnt.keyup();
					reYr.keyup();
					toDay.keyup();
					toMnt.keyup();
					toYr.keyup();
							
				</script>
			<?php
		}
	}
?>