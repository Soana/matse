<?php
	/*
	 * This will display the form for uploading a new Homework and process the postdata received from the user
	 */
	class ContentUploadHomework extends Content
	{
		public function printH1()
		{
			echo("Lösung hochladen");
		}
		/*
		 * Returns the fileending of the supplied filename 
		 */
		private function getFileEnding($name)
		{
			$exp = explode(".", $name); //Explode it on the dot
			return strtolower($exp[count($exp) - 1]); //And return the last part with only lowercharacters
		}
		
		/*
		 * Will count all homework that was already uploaded to this subject by that user
		 */
		private function countHomework($hwId, $userID)
		{
			$query = $this->matse->db()->prepare("SELECT ID FROM UploadsHomework WHERE Homework = ? AND Author = ?"); //Select it from the database
			$query->bind_param("ii", $hwId, $userID);
			$query->execute();
			$query->store_result();
			$count = $query->num_rows(); //And count the rows
			$query -> close();
			return $count + 1;
		}
		
		private function upload($userID, $name)
		{
			$hwID = $_GET["homework"];//First grep all attributes and store them into variables for faster access later on
			$file = $_FILES["file"];
			$end = $this->getFileEnding($file["name"]); //Get the fileneding of the uploaded file
			
			if(		$file["type"] == "application/pdf" //The mimtype of the file should only be PDF or ZIP
				|| 	$file["type"] == "multipart/x-zip"
				|| 	$file["type"] == "application/zip"
				|| 	$file["type"] == "application/x-zip-compressed"
				|| 	$file["type"] == "application/x-compressed")
			{
				if($file["size"] < 20 * 1024 * 1024) //The file cannot be larger than 20 megabytes
				{
					if($end == "pdf" || $end == "zip") //The ending of the file may only be .pdf or .zip 
					{
						/*** Build the filename ***/
						$subjectName = $this->matse->getSubjectNameByHomework($hwID);
						/*** Subjectname_Homeworkname_Username_Number of Files uploaded to this Homework_.fileending ***/
						$fname = "upload/homework/".$this->matse->getCleanName($subjectName)."_".$this->matse->getHomeworkName($hwID)."_".$name."_".$this->countHomework($hwID, $userID).".".$end;
						if(! is_dir("upload/homework")){
							mkdir("upload");
							mkdir("upload/homework");
						}
						move_uploaded_file($file["tmp_name"], $fname); //Now copy the uploaded file
						$query = $this->matse->db()->prepare("INSERT INTO UploadsHomework (Author, Homework, Date, Path) VALUES (?, ?, ?, ?)"); //And enter the data into the database
						$now = time(); //Current timestamp
						$query -> bind_param("iiis", $userID, $hwID, $now, $fname);
						$query -> execute();
						$query->close();
						displaySuccess("Ihre Lösung wurde erfolgreich hochgeladen! Sie kann <a href=\"$fname\">hier</a> heruntergeladen werden.");
						$this -> sendMails($hwID);
					}
					else displayError("Die Datei hat eine ungültige Endung: \"$end\", zulässig sind nur *.pdf und *.zip");
				}
				else displayError("Die Datei ist zu groß. Es sind nur Dateien mit einer maximalen Größe von 20 MB erlaubt.");
				
			}
			else displayError("Dieser Dateityp ist nicht erlaubt: ".$file["type"].", erlaubt sind nur PDF und Zip");
		}
		
		private function sendMails($hwID)
		{
			$usern = $this -> matse -> getUsername($this -> matse -> user -> userID);
			$subject = $this -> matse -> getSubjectNameByHomework($hwID);
			$mysqli = $this -> matse -> db() -> prepare("SELECT u.Mail FROM ObservedSubjects o LEFT JOIN Users u ON o.User = u.ID WHERE o.Subject = (SELECT Subject FROM Homework WHERE ID = ?)");
			$mysqli -> bind_param("i", $hwID);
			$mysqli -> execute();
			$mysqli -> bind_result($mail);
			while($mysqli -> fetch())
			{
				$this -> matse -> smtpClient -> sendMail($mail, "Aktivität im Fach \"".$subject."\"",
						"Hallo,\r\n\r\n".
						"Sie erhalten diese Mail, weil Sie ausgewählt haben, das Fach \"".$subject."\" zu beobachten.\r\n".
						"Der Benutzer $usern hat eine neue Lösung zu einer Hausaufgabe in dem entsprechenden Fach hochgeladen.\r\n".
						"Besuchen Sie den Topic, um die Lösung anzusehen.\r\n\r\n".
						"Ihre Matse-Börse");
			}
			$mysqli -> close();
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if($this->matse->user->loggedIn) //if there is an logged-in user, use his name and userid
			{
				$userID = $this->matse->user->userID;
				$name = $this->matse->user->username;
			}
			else //Else use "Anonymous" and the nonexistent userid 0
			{
				/*$name = "Anonymous";
				$userID = 0;*/
				displayError("Sie müssen angemeldet sein, um dies zu tun");
			}
			if(isset($_FILES["file"])) //If we received a file we may process it
			{
				$this -> upload($userID, $name);
			}
			?>
				<form action="#" method="POST" enctype="multipart/form-data">
					<p>
						<label>Benutzer</label>
						<div class="fakeInput"><?php echo($name); ?></div>
					</p>
					<p>
						<label>Datei</label>
						<input type="file" name="file" />
					</p>
					<p>
						<label>Hochladen</label>
						<input value="Okay" type="submit"/>
						<div class="uploadInfo">Es sind nur Dateien mit einer Größe von &lt; 20MB erlaubt sowie nur PDF- un ZIP-Dateien</div>
					</p>
				</form>
			<?php
		}
	}
?>