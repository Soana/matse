<?php 

	class ContentChat extends Content
	{
		
		public function printH1()
		{
			echo("Chat");
		}
		public function printHTML()
		{
			?>
				<!-- <div class="chatLoader" id="loader">
					<img src="style/ajax-loader.gif" />
				</div> -->
				<div class="chatUsers" id="users"></div>
				<div class="chatBox" id="chat"></div>
				<div class="chatInput">
					<textarea id="message">Befehle:&#10;[latex] Latex-Code Hier [/latex]&#10;[code=lang] Sourcecode hier [/code]&#10;Verfügbare Sprachen: c++, java, js, python, php, sql, c#&#10;STRG+Enter oder Shift+Enter erzeugen einen Zeilenumbruch&#10;Enter schickt die Nachricht ab</textarea>
				</div>
				<script type="text/javascript">
					var load = $('#loader');
					window.onbeforeunload = function()
					{
						$.ajax(
						{
							url: "chat.php?ac=logout",
							cache: false
						});
					};
					$().ready(function()
					{
						$.ajax(
						{
							url: "chat.php?ac=login",
							cache: false
						});
					});
					var init = false;
					$('#message').click(function()
					{
						if(!init)
						{
							$('#message').css("color", "black");
							$('#message').css("font-style", "normal");
							$('#message').val("");
							init = true;
						}
					});
					$('#message').keypress(function(e)
					{
						if(e.which === 13)
						{
							return false;
						}
					});
					$('#message').keyup(function(e)
					{
						if(e.which == 13 && ! (e.ctrlKey || e.shiftKey) )
						{
							sendChat();
							return false;
						}
						else if((e.ctrlKey || e.shiftKey) && e.which == 13)
						{
							var content = $('#message').val();
							var position = $("#message").getCursorPosition();
							var newContent = content.substr(0, position) + "\n" + content.substr(position);
							$('#message').val(newContent);
							$('#message').setCursorPosition(position + 1);
							//$('#message').val($('#message').val() + "\n")
						}
							
					});

					function sendChat()
					{
						load.css("display", "block");
						var raw = $('#message').val();
						$('#message').val("");
						var request = $.ajax(
						{
							url: "chat.php?ac=send",
							cache: false,
							dataType: 'json',
							type: "post",
							data: {msg: raw}
						}).done(function (response, textStatus, jqXHR)
						{
							load.css("display", "none");
						});
						
					}

					var lastID = 0;
					
					function reloadChat()
					{
						load.css("display", "block");
						$.ajax(
						{
							url: "chat.php?ac=recv&id="+lastID,
							cache: false
						}).done(function(re) 
						{
							load.css("display", "none");
							if(re.length != 0)
							{
								var lines = re.split("<next>");
								var chat =$('#chat'); 
								for(var i = lines.length - 1; i >= 0; i--)
								{
									var parts = lines[i].split(",");
									if(parts.length == 4)
									{
										var date = new Date(parseInt(parts[2]) * 1000);
										var h = date.getHours();
										var m = date.getMinutes();
										var s = date.getSeconds();
										if(h < 10) h = "0"+h;
										if(m < 10) m = "0"+m;
										if(s < 10) s = "0"+s;
										var dateTxt = h+":"+m+":"+s;
										var id = parseInt(parts[0]);
										lastID = id;
										var nM = $('<div class="chatMsg"></div>');
										nM.append($('<div class="date">'+dateTxt+'</div>'));
										nM.append($('<div class="user"><a href="?action=viewUser&name='+parts[3]+'">'+parts[3]+'</a></div>'));
										//nM.append($('<div class="user">'+parts[3]+'</div>'));
										var txt = $('<div class="msg" id="msg'+id+'">'+/*replaceURLWithHTMLLinks(*/parts[1]/*)*/+'</div><div class="chatclear"></div>');
										nM.append(txt);
										chat.append(nM);
										MathJax.Hub.Queue(["Typeset",MathJax.Hub,"msg"+id]);
										SyntaxHighlighter.highlight();
									}
								}
								chat.animate({ scrollTop: chat[0].scrollHeight },'50');
							}
							setTimeout(reloadChat, 1800);
						});
					}

					function replaceURLWithHTMLLinks(text) 
					{
						var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
						return text.replace(exp,"<a href='$1'>$1</a>"); 
					}

					function reloadUsers()
					{
						load.css("display", "block");
						$.ajax({
							url: "chat.php?ac=users",
							cache: false
						}).done(function(re) 
						{
							load.css("display", "none");
							var users = re.split("\n");
							var box = $('#users');
							box.html("<h2>Benutzer im Chat</h2>");
							var ul = $("<ul></ul>");
							for(var i = 0; i < users.length; i++)
								if(users[i].length != 0) ul.append($('<li>'+users[i]+'</li>'));
							box.append(ul);
							setTimeout(reloadUsers, 8000);
						});
					}

					reloadUsers();
					reloadChat();
				</script>
			<?php
		}
	}

?>
