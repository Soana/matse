<?php
	class ContentAjaxMenu extends Content
	{
		public function printH1()
		{
			
		}
		
		public function printHTML()
		{
			if(empty($_GET["sem"]) || !$_GET["sem"])
			{
				?>
					<ul>
						<li id="sem1"><a href="javascript: toggleSubjectMenu(1);">Semester I</a></li>
						<li id="sem2"><a href="javascript: toggleSubjectMenu(2);">Semester II</a></li>
						<li id="sem3"><a href="javascript: toggleSubjectMenu(3);">Semester III</a></li>
						<li id="sem4"><a href="javascript: toggleSubjectMenu(4);">Semester IV</a></li>
						<li id="sem5"><a href="javascript: toggleSubjectMenu(5);">Semester V</a></li>
						<li id="sem6"><a href="javascript: toggleSubjectMenu(6);">Semester VI</a></li>
						<li id="sem7"><a href="javascript: toggleSubjectMenu(7);">Kurse</a></li>
					</ul>
				<?php
			}
			else if(empty($_GET["sub"]) || !$_GET["sub"])
			{
				$_SESSION["sem"] = $_GET["sem"];
				$query = $this -> matse->db()->prepare("SELECT ID, Name FROM Subjects WHERE Semester = ?");
				$query->bind_param("i", $_GET["sem"]);
				$query -> execute();
				$query-> bind_result($id, $name);
				?>
					<ul>
				<?php
				while($query->fetch())
				{
					?>
							<li><a href="?action=subjectView&subject=<?php echo($id); ?>"><?php echo($name); ?></a></li>
					<?php
				}
				$query->close();
				if($this -> matse -> user -> hasPrivileg("CreateSubject"))
				{
					?>
						<li><a href="?action=subjectCreate&semester=<?php echo($_GET["sem"]); ?>">Neu</a></li>
					<?php
				}
				?>
					</ul>
				<?php
			}
			else
			{
				$_SESSION["sem"] = $_GET["sem"];
				$_SESSION["sub"] = $_GET["sub"];
				?>
					<ul>
						<li><a href="?action=subjectView&subject=<?php echo($_GET["sub"]); ?>">Hausaufgaben</a></li>
					</ul>
				<?php
			}
		}
	}
?>