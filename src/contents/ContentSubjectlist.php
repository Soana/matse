<?php 

	/*
	 * This content will list all subjects that are known to the database
	 */
	class ContentSubjectlist extends Content
	{
		public function printH1()
		{
			echo("Fächer");
		}
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			?>
				<table>
					<tr class="head">
						<td>Name</td>
						<td>Betreten</td>
					</tr>
					<?php
						$query = $this->matse->db()->prepare("SELECT ID, Name FROM Subjects"); //Grep all subjects from the database
						$query->execute();
						$query->bind_result($id, $name);
						while($query->fetch()) //And generate a table with each row a dataset from it
						{
							?>
								<tr>
									<td><?php echo($name); ?></td>
									<td><a href="?action=subjectView&subject=<?php echo($id); ?>">Hausaufgaben</a></td>
									<td><a href="?action=subjectViewLiterature&subject=<?php echo($id); ?>">Literatur</a></td>
								</tr>
							<?php
						}
						$query->close();
					?>
				</table>
				<br>
				<?php 
					if($this->matse->user->hasPrivileg("CreateSubject")) //If the user has the privileg to create a new subject, display him the link
					{
						?>
							<a href="?action=subjectCreate">hier</a> kann ein neues Fach angelegt werden.
						<?php
					}
				?>
			<?php 
		}
	}
?>