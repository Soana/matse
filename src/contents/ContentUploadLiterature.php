<?php
	/*
	 * This will display the form for uploading a new Homework and process the postdata received from the user
	 */
	class ContentUploadLiterature extends Content
	{
		public function printH1()
		{
			echo("Literatur hochladen");
		}
		/*
		 * Returns the fileending of the supplied filename 
		 */
		private function getFileEnding($name)
		{
			$exp = explode(".", $name); //Explode it on the dot
			return strtolower($exp[count($exp) - 1]); //And return the last part with only lowercharacters
		}
		
		/*
		 * Will count all homework that was already uploaded to this subject by that user
		 */
		private function countLiterature($userID)
		{
			$query = $this->matse->db()->prepare("SELECT ID FROM Literature WHERE Author = ?"); //Select it from the database
			$query->bind_param("i", $userID);
			$query->execute();
			$query->store_result();
			$count = $query->num_rows(); //And count the rows
			$query -> close();
			return $count + 1;
		}
		
		private function upload($userID, $name)
		{
			$lname = $_POST["lname"];
			$type = $_POST["type"];
			$file = $_FILES["file"];
			
			$end = $this->getFileEnding($file["name"]); //Get the fileneding of the uploaded file
			if(		$file["type"] == "application/pdf" //The mimtype of the file should only be PDF or ZIP
				|| 	$file["type"] == "multipart/x-zip"
				|| 	$file["type"] == "application/zip"
				|| 	$file["type"] == "application/x-zip-compressed"
				|| 	$file["type"] == "application/x-compressed")
			{
				if($file["size"] < 20 * 1024 * 1024) //The file cannot be larger than 20 megabytes
				{
					if($end == "pdf" || $end == "zip") //The ending of the file may only be .pdf or .zip 
					{
						/*** Build the filename ***/
						$subjectName = $this->matse->getSubjectName($_GET["subject"]);
						/*** Subjectname_Username_Number of Files uploaded to this Homework_.fileending ***/
						$fname = "upload/literature/".$this->matse->getCleanName($subjectName)."_".$this->matse->getCleanName($lname)."_".$this->countLiterature($userID).".".$end;
						if(! is_dir("upload/literature")){
							@mkdir("upload");
							@mkdir("upload/literature");
						}
						move_uploaded_file($file["tmp_name"], $fname); //Now copy the uploaded file
						$query = $this->matse->db()->prepare("INSERT INTO Literature (Author, Type, Date, Path, Name, Subject) VALUES (?, ?, ?, ?, ?, ?)"); //And enter the data into the database
						$now = time(); //Current timestamp
						$query -> bind_param("iiissi", $userID, $type, $now, $fname, $lname, $_GET["subject"]);
						$query -> execute();
						$query->close();
						$this -> sendMails();
						displaySuccess("Ihre Literatur wurde erfolgreich hochgeladen! Sie kann <a href=\"$fname\">hier</a> heruntergeladen werden.");
					}
					else displayError("Die Datei hat eine ungültige Endung: \"$end\", zulässig sind nur *.pdf und *.zip");
				}
				else displayError("Die Datei ist zu groß. Es sind nur Dateien mit einer maximalen Größe von 20 MB erlaubt.");
				
			}
			else displayError("Dieser Dateityp ist nicht erlaubt: ".$file["type"].", erlaubt sind nur PDF und Zip");
		}


		private function sendMails()
		{
			$usern = $this -> matse -> getUsername($this -> matse -> user -> userID);
			$subject = $this -> matse -> getSubjectName( $_GET["subject"]);
			$mysqli = $this -> matse -> db() -> prepare("SELECT u.Mail FROM ObservedSubjects o LEFT JOIN Users u ON o.User = u.ID WHERE o.Subject = ?");
			$mysqli -> bind_param("i", $_GET["subject"]);
			$mysqli -> execute();
			$mysqli -> bind_result($mail);
			while($mysqli -> fetch())
			{
				$this -> matse -> smtpClient -> sendMail($mail, "Aktivität im Fach \"".$subject."\"",
						"Hallo,\r\n\r\n".
						"Sie erhalten diese Mail, weil Sie ausgewählt haben, das Fach \"".$subject."\" zu beobachten.\r\n".
						"Der Benutzer $usern hat eine neue Literatur in dem entsprechenden Fach hochgeladen.\r\n".
						"Besuchen Sie den Topic, um die Literatur anzusehen.\r\n\r\n".
						"Ihre Matse-Börse");
			}
			$mysqli -> close();
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if($this->matse->user->loggedIn) //if there is an logged-in user, use his name and userid
			{
				$userID = $this->matse->user->userID;
				$name = $this->matse->user->username;
			}
			else //Else use "Anonymous" and the nonexistent userid 0
			{
				/*$name = "Anonymous";
				$userID = 0;*/
				displayError("Sie müssen angemeldet sein, um dies zu tun.");
				return;
			}
			if(isset($_FILES["file"])) //If we received a file we may process it
			{
				$this -> upload($userID, $name);
			}
			?>
				<form action="#" method="POST" enctype="multipart/form-data">
					<p>
						<label>Benutzer</label>
						<div class="fakeInput"><?php echo($name); ?></div>
					</p>
					<p>
						<label>Name</label>
						<input type="text" name="lname" />
					</p>
					<p>
						<label>Datei</label>
						<input type="file" name="file" />
					</p>
					<p>
						<label>Typ</label>
						<select name="type">
							<option value="1">Zusammenfassung</option>
							<option value="2">Skript</option>
							<option value="3">Übung</option>
							<option value="4">Lösung</option>
							<option value="5">Sonstiges</option>
						</select>
					</p>
					<p>
						<label>Hochladen</label>
						<input value="Okay" type="submit"/>
					</p>
					<div class="uploadInfo">Es sind nur Dateien mit einer Größe von &lt; 20MB erlaubt sowie nur PDF- und ZIP-Dateien</div>
					<br>
					<div class="uploadInfo"><b>Bitte beachten:</b> Laden sie <b>ausschliesslich</b> Dateien hoch, die Sie selbst erstellt haben, die unter einen offenen Lizenz (bspw. GPL, CC, ...) stehen, oder zu deren Veröffentlichung sie <b>ausdrücklich</b> berechtigt wurden. Danke.</div>
				</form>
			<?php
		}
	}
?>