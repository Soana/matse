<?php
	/*
	 * Makes it possible to register a new account on this server 
	 */
	class ContentRegister extends Content
	{

		public function printH1()
		{
			echo("Neuen Account anlegen");
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if(isset($_POST["nickname"])) //If this is already set -> we have received postdata and may process it now
			{
				$nickname = $_POST["nickname"];//Grep all arguments for faster access
				$password = $_POST["password"];
				$repeat = $_POST["repeat"];
				$location = $_POST["location"];
				$okay = true; //Will be overrided lateron if anything went wrong
				if(strlen($nickname) < 3) //Was the username too short?
				{
					$okay = false; // <- Went wrong
					?>
						<div class="error">
							<h1>Fehler</h1>
							Der angegebene Nickname ist zu kurz.
						</div>
					<?php 
				}
				if($this->matse->usernameTaken($nickname)) //Was the username already taken?
				{
					$okay = false;// <- Went wrong
					?>
						<div class="error">
							<h1>Fehler</h1>
							Der angegebene Nickname wird bereits verwendet.
						</div>
					<?php 
				}
				if(strlen($password) < 6) //The password was too short
				{
					$okay = false;// <- Went wrong
					?>
						<div class="error">
							<h1>Fehler</h1>
							Das angegebene Passwort ist zu kurz.
						</div>
					<?php 
				}
				if($password != $repeat) //The two passwords didn't match
				{
					$okay = false;// <- Went wrong
					?>
						<div class="error">
							<h1>Fehler</h1>
							Die beiden Passwörter stimmen nicht überein.
						</div>
					<?php 
				}
				if($okay)//If nothign went wrong:
				{
					$password = sha1($password); //First: hash the password. Passwords will only be handled hashed from now on
					$query = $this->matse->db()->prepare("INSERT INTO Users (Name, Password, Location) VALUES (?, ?, ?)"); //Insert the user into the database
					$query -> bind_param("ssi", $nickname, $password, $location);
					$query -> execute();
					$query -> store_result();
					if($query -> insert_id == 1) //If this is the first user at all entered into the database he will get the privileges to change other users privileges
					{
						?>
							<div class="success">
								<h1>Erfolgreich</h1>
								Sie sind der erste registrierte Benutzer und haben das Recht zum verwalten von Privilegien erhalten.
							</div>
						<?php 
						$query2 = $this->matse->db()->prepare("INSERT INTO Privileges (User, Privileg) VALUES (1, ?)"); //Give him the Privilege to change other users privileges
						$mng = $this->matse->getPrivileg("ManagePrivileges")->id;
						$query2 -> bind_param("i", $mng);
						$query2 -> execute();
						$query2 -> close();
					}
					$query -> close();
					?>
						<div class="success">
							<h1>Erfolgreich</h1>
							Ihr Account wurde erfolgreich angelegt. In 1 Sekunde geht es weiter. Viel Vergnügen.
							<script type="text/javascript">
								setTimeout(function(){location.replace("index.php?action=start")}, 1000);
							</script>
							
						</div>
					<?php 
				}
			}
			?>
				<form action="#" method="POST">
					<p>
						<label>Nickname</label>
						<input name="nickname" id="nickname" type="text"/>
						<span id="nicknameInfo" class="info"></span>
					</p>
					<p>
						<label>Passwort</label>
						<input name="password" id="password" type="password"/>
						<span id="passwordInfo" class="info"></span>
					</p>
					<p>
						<label>Wiederholen</label>
						<input name="repeat" id="repeat" type="password"/>
						<span id="repeatInfo" class="info"></span>
					</p>
					<p>
						<label>Standort</label>
						<input name="location" id="location" type="radio" value="0" checked="checked"/>Aachen<br>
						<input name="location" id="location" type="radio" value="1"/>Jülich<br>
						<input name="location" id="location" type="radio" value="2"/>Köln<br>
						<span id="locationInfo" class="info"></span>
					</p>
					<p>
						<label>Speichern</label>
						<input id="submit" type="submit" value="Okay"/>
						<span id="okayInfo" class="info"></span>
					</p>
				</form>
				<script type="text/javascript">
					var submit = $('#submit');
					var submitInfo = $('#okayInfo');
					submit.check = function()
					{
						if(nickname.okay && password.okay && repeat.okay)
						{
							submit.removeAttr("disabled");
							submitInfo.text("Okay");
							submitInfo.addClass("okay");
							submitInfo.removeClass("bad");
						}
						else
						{
							submit.attr("disabled", "disabled");
							submitInfo.text("Bitte alles nochmals prüfen");
							submitInfo.removeClass("okay");
							submitInfo.addClass("bad");
						}	
					};
					var nickname = $('#nickname');
					nickname.keyup(function() 
					{
						var info = $('#nicknameInfo');
						if(nickname.val().length < 3) 
						{
							info.text("Zu kurz");
							info.removeClass("okay");
							info.addClass("bad");
							nickname.okay = false;
						}
						else
						{
							info.text("Okay");
							info.addClass("okay");
							info.removeClass("bad");
							nickname.okay = true;
						}
						submit.check();
					});
					var password = $('#password');
					password.keyup(function() 
					{
						var info = $('#passwordInfo');
						if(password.val().length < 6) 
						{
							info.text("Zu kurz");
							info.removeClass("okay");
							info.addClass("bad");
							password.okay = false;
						}
						else
						{
							info.text("Okay");
							info.addClass("okay");
							info.removeClass("bad");
							password.okay = true;
						}
						submit.check();
					});
					var repeat = $('#repeat');
					repeat.keyup(function() 
					{
						var info = $('#repeatInfo');
						if(repeat.val() != password.val()) 
						{
							info.text("Stimmt nicht überein");
							info.removeClass("okay");
							info.addClass("bad");
							repeat.okay = false;
						}
						else
						{
							info.text("Okay");
							info.addClass("okay");
							info.removeClass("bad");
							repeat.okay = true;
						}
						submit.check();
					});
				</script>
			<?php
		}
	}
?>
