<?php
	/*
	 * This will display a form to create a new subject
	 */
	class ContentSubject extends Content
	{
		public function printH1()
		{
			echo("Neues Fach anlegen");
		}
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if(!$this->matse->user->hasPrivileg("CreateSubject")) //If the user doesn't have the privileg to do this...
			{
				?>
					<div class="error">
						<h1>Fehler</h1>
						Unberechtigter Zugriff!
					</div>
				<?php 
				return;//Kick him out
			}
			if(isset($_POST["name"])){ //If we have already received some post-data we may compute it
				$name = $_POST["name"]; //First grep all arguments for faster access
				$desc = $_POST["description"];
				$sem = $_POST["semester"];
				$okay = true; //Will be overridden if anything went wrong
				if($this->nameTaken($name)){ //The name of this subject was already taken
					$okay = false; // <- Went wrong
					?>
						<div class="error">
							<h1>Fehler</h1>
							Das angegebene Fach <?php echo $name?> existiert bereits.
						</div>
					<?php 
				}
				if(!is_int($sem+0) || $sem < 1 || $sem > 7){ //The name of this subject was already taken
					$okay = false; // <- Went wrong
					?>
						<div class="error">
							<h1>Fehler</h1>
							<?php echo($sem); ?> ist keine Zahl zwischen 1 und 6
						</div>
					<?php 
				}
				if($okay)
				{ //If everything went right
					$query = $this->matse->db()->prepare("INSERT INTO Subjects (Semester, Name, Description) VALUE (?, ?, ?)"); //Insert the subject into the database
					$query->bind_param("iss", $sem, $name, $desc);
					$query->execute();
					$query -> store_result();
					$query->close();
					?>
						<div class="success">
							<h1>Erfolgreich</h1>
							Das Fach <?php echo $name?> wurde erfolgreich angelegt.
						</div>
					<?php
				} 
			}
			?>
			
				<form action="#" method="POST">
					<p>
						<label>Name</label>
						<input name="name" id="name" type="text"/>
					</p>
					<p>
						<label>Beschreibung</label>
						<textarea name="description" id="description" rows="4" cols="50"/></textarea>
					</p>
					<input name="semester" id="semester" type="hidden" value="<?php echo($_GET["semester"]);?>"/>
					<p>
						<label>Speichern</label>
						<input id="submit" type="submit" value="Okay"/>
					</p>
				</form>
			<?php 
		}
		
		/*
		 * Checks, whether the name of a subject is already taken or whether it is still available
		 */
		public function nameTaken($name)
		{
			$query = $this->matse->db()->prepare("SELECT ID FROM Subjects WHERE Name=?");//Look the name up in the database
			$query->bind_param("s", $name);
			$query->execute();
			if($query->fetch()){ //If it does not return an empty result there was a dataset to it and thous it was already taken
				$query->close();
				return true;
			}
			else{ //Else it it is still available
				$query->close();
				return false;
			}
			
		}
	}
?>