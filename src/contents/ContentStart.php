<?php
	/*
	 * This should contain some description about this webpage
	 */
	class ContentStart extends Content
	{
		/*
		 * Render the page
		 */
		private function getQuestionCount()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM Questions");
			$mysqli -> execute();
			$mysqli -> store_result();
			$num = $mysqli -> num_rows;
			$mysqli -> close();
			return $num;
		}
		
		private function getAnswerCount()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM Answers");
			$mysqli -> execute();
			$mysqli -> store_result();
			$num = $mysqli -> num_rows;
			$mysqli -> close();
			return $num;
		}
		
		private function getUploadCount()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM UploadsHomework");
			$mysqli -> execute();
			$mysqli -> store_result();
			$num = $mysqli -> num_rows;
			$mysqli -> close();
			$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM Literature");
			$mysqli -> execute();
			$mysqli -> store_result();
			$num += $mysqli -> num_rows;
			$mysqli -> close();
			return $num;
		}
		
		private function getUserCount()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM Users");
			$mysqli -> execute();
			$mysqli -> store_result();
			$num = $mysqli -> num_rows;
			$mysqli -> close();
			return $num;
		}
		

		public function printH1()
		{
			echo("Matse-Börse");
		}
		
		
		public function printHTML()
		{
			?>
				<img src="img/beta.png" style="float: right;"/>
				<h2>Über die Matse-Börse</h2>
				<p>Die Matse-Börse dient dem Austausch von Informationen und dem gegenseitigen Helfen, gemeinsamen Lernen und Informationsaustausch.</p>
				<br>
				<p>Sie bietet eine Vielzahl von Möglichkeiten, euch den Wissensaustausch zu erleichtern, bspw. einen Webchat mit LaTeX und Sourcecode-Highlighting, ein Uploadarchiv und ein Forum</p>
				<p>Jeder kann sofort mitmachen: Einfach registrieren, einloggen und los gehts!</p>
				<br>
				<p>Die Matse-Börse befindet sich derzeit noch in aktiver Entwicklung und wir sind immer offen für Fragen, Anregungen und selbstervständlich Bug- und Securityissuereports.</p>
				<p>Das gesamte Projekt ist Opensource und freut sich unter dem git-Repository im Footer auf eure Mitarbeit!</p>
				<h2>Zahlen</h2>
				<p>Die Matse-Börse umfasst derzeit <b><?php echo($this -> getQuestionCount()); ?></b> Fragen und <b><?php echo($this -> getAnswerCount()); ?></b> Antworten.</p>
				<p>Ihr könnt <b><?php echo($this -> getUploadCount()); ?></b> Dateien aus unserem Archiv herunterladen.</p>
				<p>Es haben sich bisher <b><?php echo($this -> getUserCount()); ?></b> Benutzer registriert.</p>
				
			<?php
		}
	}
?>
