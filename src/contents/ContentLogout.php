<?php
	/*
	 * Allows the user to log himself out
	 */
	class ContentLogout extends Content
	{
		public function printH1()
		{
			echo("Abmelden");		
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			session_destroy(); //Simply destroy the session and thous delete the logininformation and everything else
			displaySuccess("Sie wurden erfolgreich abgemeldet. In 1 Sekunde geht es weiter. Schönen Tag noch.");
			?>
				<script type="text/javascript">
					setTimeout(function(){location.replace("index.php?action=login")}, 1000);
				</script>
			<?php 
		}
	}

?>