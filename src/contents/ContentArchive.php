<?php
require_once("src/Zip.php");

class ContentArchive extends Content{
	
	public function printH1(){
		echo ("Archivieren von ".$this->matse->getSubjectname($_GET["subject"]));	
		
	}
	
	public function archive(){
		$filename = "./archive/";
		if($_POST["month"] < 4){
			$filename .= "Wintersemester_".$_POST["year"]."-".($_POST["year"]+1);
		}
		else{
			$filename .= "Sommersemester_".$_POST["year"]; 
		}
		$filename .= "_".$this->matse->getCleanName($this->matse->getSubjectName($_GET["subject"])).".zip";
		
		$zip = new Zip();
		$zip->setZipFile($filename);
		//deleate Homework
		$links =  array();
		$time = mktime(0,0,0,$_POST["month"], $_POST["day"], $_POST["year"]);
		$query = $this->matse->db()->prepare("SELECT ID, Name, File, Location FROM Homework WHERE Subject = ? AND Due <= ?");
		$query->bind_param("ii", $_GET["subject"], $time);
		$query->execute();
		$query->bind_result($id, $name, $file, $location);
		while($query->fetch()){
			$links[$name]["location"] = $location;
			$links[$name]["file"] = $file;
			$links[$name]["id"] = $id;
		}
		$query->close();
		//deleate Uploads
		$data = "In dieser Datei stehen alle Hausaufgaben und die Links zu den Uebungsblaettern.\r\n"
				."Wir koennen leider nicht garantieren, das die Links immernoch gueltig sind oder auf die gleiche Datei zeigen.\r\n"
				."Wir empfehlen sofort nach Erhalt diser Datei, allen Links zu folgen und die ensprechenden Uebungsblaetter herunterzuladen.\r\n"
				."Wir uebernehmen keine Garantie fuer die Inhalte der Links oder die bereitgestellten Loesungen.\r\n\r\n"
				."Ort\tName\tLink\r\n";
		foreach($links as $key=>$values){
			switch($values["location"]){
				case 0: $values["location"] = "Aachen";
						break;
				case 1: $values["location"] = "Juelich";
						break;
				case 2: $values["location"] = "Koeln";
						break;
			}
			$data .= $values["location"]."\t".$key."\t".$values["file"]."\r\n";
			
			$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Homework WHERE ID = ?");
			$mysqli -> bind_param("i", $values["id"]);
			$mysqli -> execute();
			$mysqli -> close();
			$mysqli = $this -> matse -> db() -> prepare("SELECT Path FROM UploadsHomework WHERE Homework = ?");
			$mysqli -> bind_param("i", $values["id"]);
			$mysqli -> execute();
			$mysqli -> bind_result($path);
			while($mysqli -> fetch())
			{
				//put uploads to zip
				$newPath = array();
				$newPath = explode("/", $path);
				$zip->addFile(file_get_contents("./".$path), end($newPath));
				@unlink($path);
			}
			$mysqli -> close();
			$mysqli = $this -> matse -> db() -> prepare("DELETE FROM UploadsHomework WHERE Homework = ?");
			$mysqli -> bind_param("i", $values["id"]);
			$mysqli -> execute();
			$mysqli -> close();
			
		}
		
		$zip->addFile($data, "Aufgaben.txt");
		//make zip file
		$zip->finalize();
		
		displaySuccess("Das Fach ".$this->matse->getSubjectName($_GET["subject"])." wurde erfolgreich archviert.");
	}
	
	public function printHTML(){
		
		if(isset($_POST["sure"])){
			$this->archive();
		}
		else{
			$year = date("Y"); //Calculate current time to write into the form
			$month = date("m");
			$day = date("d");
			?>
			<form action="?action=subjectView&subject=<?php echo $_GET["subject"];?>" method="POST">
				Archiviere alle Einträge bis zum:<br>
				<input name="day" type="text" id="day" style="width: 2em" value="<?php echo($day);?>">.
				<input name="month" type="text" id="month" style="margin-left:0; width: 2em" value="<?php echo($month);?>">.
				<input name="year" type="text" id="year" style="margin-left:0; width: 4em" value="<?php echo($year);?>">
				<br><br>
				Sind Sie wirklich sicher, dass Sie das Fach <?php echo ($this->matse->getSubjectname($_GET["subject"]));?>
				bis zum angegebenen Zeitpunkt archivieren wollen?<br>
				Diese Aktion kann nicht wieder rückgängig gemacht werden!<br>
				<input type="Submit" name="sure" id="sure" value="Ja, ich bin mir sicher">&nbsp&nbsp&nbsp&nbsp
			</form>
			<form action="?action=subjectView&subject=<?php echo $_GET["subject"];?>" method="POST">
				<input type="Submit" name="notsure" id="notsure" value="Nein, ich bin mir nicht sicher">
			</form>
		<?php 
		}
		
	}
}
?>