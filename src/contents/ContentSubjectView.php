<?php
	/*
	 * This Content displays a particular subject and all homeworks an uploads that belong to it
	 */
	class ContentSubjectView extends Content
	{
		public function printH1()
		{
			$name = $this->getSubjectName($_GET["subject"]); //Get the name of the subject and use it as headline
			echo($name);
		}
		/*
		 * Returns the name of a subject by its ID
		 */
		private function getSubjectName($id)
		{
			$query = $this->matse->db()->prepare("SELECT Name FROM Subjects WHERE ID = ?"); //First get the name of the subject
			$query->bind_param("i", $id);
			$query->execute();
			$query->bind_result($name);
			$query->fetch();
			$query->close();
			return $name;
		}
		
		private function deleteHomework()
		{
			if($this -> matse -> user -> hasPrivileg("DeleteHomework"))
			{
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Homework WHERE ID = ?");
				$mysqli -> bind_param("i", $_GET["deleteHomework"]);
				$mysqli -> execute();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("SELECT Path FROM UploadsHomework WHERE Homework = ?");
				$mysqli -> bind_param("i", $_GET["deleteHomework"]);
				$mysqli -> execute();
				$mysqli -> bind_result($path);
				while($mysqli -> fetch())
				{
					@unlink($path);
				}
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM UploadsHomework WHERE Homework = ?");
				$mysqli -> bind_param("i", $_GET["deleteHomework"]);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Alle Uploads dieser Kategorie und die Kategorie selber wurden gelöscht.");
			}
			else
				displaySuccess("Sie haben keine Berechtigung, dies zu tun.");
		}
		
		private function vote()
		{
			if($this -> matse -> user -> loggedIn)
			{
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Reputations WHERE Voter = ? AND Item = ? AND ItemType = 1");
				$mysqli -> bind_param("ii", $this -> matse -> user -> userID, $_GET["voteUpload"]);
				$mysqli -> execute();
				$mysqli -> close();
				if($_GET["score"] == "1") $score = 1;
				else $score = -1;
				$mysqli = $this -> matse -> db() -> prepare("SELECT u.Author, h.Subject FROM UploadsHomework u LEFT JOIN Homework h ON h.ID = u.Homework WHERE u.ID = ?");
				$mysqli -> bind_param("i", $_GET["voteUpload"]);
				$mysqli -> execute();
				$mysqli -> bind_result($user, $subject);
				$mysqli -> fetch();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("INSERT INTO Reputations (User, Voter, Item, ItemType, Subject, Score) VALUES (?, ?, ?, 1, ?, ?)");
				$mysqli -> bind_param("iiiii", $user, $this -> matse -> user -> userID, $_GET["voteUpload"], $subject, $score);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Lösung wurde erfolgreich mit $score bewertet.");
			}
			else
			{
				displayError("Sie müssen angemeldet sein, um dies zu tun.");
			}
		}
		
		private function deleteUpload()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT Author, Path FROM UploadsHomework WHERE ID = ?");
			$mysqli -> bind_param("i", $_GET["deleteUpload"]);
			$mysqli -> execute();
			$mysqli -> bind_result($userid, $path);
			$mysqli -> fetch();
			$mysqli -> close();
			if($userid == $this -> matse -> user -> userID || $this -> matse -> user -> hasPrivileg("DeleteUpload"))
			{
				@unlink($path);
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM UploadsHomework WHERE ID = ?");
				$mysqli -> bind_param("i", $_GET["deleteUpload"]);
				$mysqli -> execute();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Reputations WHERE Item = ? AND ItemType = 1");
				$mysqli -> bind_param("i", $_GET["deleteUpload"]);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Die Lösung wurde erfolgreich gelöscht.");
			}
			else
			{
				displayError("Sie haben keine Berechtigung, diesen Upload zu löschen.");
			}
		}
		
		private function observe()
		{
			$subj = $this->getSubjectName($_GET["subject"]);
			$mysqli = $this -> matse -> db() -> prepare("DELETE FROM ObservedSubjects WHERE Subject = ? AND User = ?");
			$mysqli -> bind_param("ii", $_GET["subject"], $this -> matse -> user -> userID);
			$mysqli -> execute();
			$mysqli -> close();
			if(isset($_POST["observe"]) && $_POST["observe"] == "true")
			{
				$mysqli = $this -> matse -> db() -> prepare("INSERT INTO ObservedSubjects(Subject,User) VALUES (?, ?)");
				$mysqli -> bind_param("ii", $_GET["subject"], $this -> matse -> user -> userID); 
				$mysqli -> execute();
				$mysqli -> close();
				$this -> matse -> smtpClient -> sendMail($this -> matse -> user->mail, "Beobachte Fach \"".$subj."\"", 
						"Hallo,\r\n\r\n".
						"Sie erhalten diese Mail, weil Sie ausgewählt haben, das Fach \"".$subj."\" zu beobachten.\r\n".
						"Sie werden zukünftig bei jedem neuen Upload (Hausaufgaben und Literaturen) per E-Mail benachrichtigt.\r\n".
						"Bitte besuchen Sie das Fach, um diese Benachrichtigung wieder zu deaktivieren.\r\n\r\n".
						"Ihre Matse-Börse");
				displaySuccess("Sie beobachten dieses Fach nun und werden per E-Mail benachrichtigt, wenn neue Hausaufgaben, Lösungen oder Literatur hochgeladen werden.<br> Eine Bestätigungs Mail wurde Ihnen zugesandt.");
			} 
			else
				displaySuccess("Sie beobachten dieses fach nicht mehr.");
			
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if(!isset($_GET["subject"])) //If there is no subject to display we don't know what to display and exit
			{
				displayError("Ein erwarteter Parameter wurde nicht übergeben!");
			}
			else
			{
				if(isset($_GET["deleteHomework"]))
				{
					$this -> deleteHomework();
				}
				if(isset($_GET["observe"]))
				{
					$this -> observe();
				}
				if(isset($_GET["voteUpload"]) && isset($_GET["score"]))
				{
					$this -> vote();
				}
				if(isset($_GET["deleteUpload"]))
				{
					$this -> deleteUpload();
				}
				$name = $this->getSubjectName($_GET["subject"]); //Get the name of the subject and use it as headline
				?>
					<p>Hochgeladene Literatur und Skripte sind <a href="?action=subjectViewLiterature&subject=<?php echo($_GET["subject"]); ?>">hier</a> zu finden </p>
						<form method="GET" action="#" id="allLocations">
							<input type="hidden" name="action" value="subjectView">
							<input type="hidden" name="subject" value="<?php echo $_GET["subject"];?>">
							<p><input type="checkbox" name="locationAll" onclick="javascript: $('#allLocations').submit();" <?php if(isset($_GET["locationAll"])){echo "checked=\"checked\"";}?>> Die Hausaufgaben aller Standorte anzeigen</p>
						</form>
				<?php
				if($this -> matse -> user -> loggedIn)
				{
					$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM ObservedSubjects WHERE User = ? AND Subject = ?");
					$mysqli -> bind_param("ii", $this -> matse -> user -> userID, $_GET["subject"]);
					$mysqli -> execute();
					if($mysqli -> fetch())
					{
						$observed = "checked";
					}
					else 
						$observed = "";
					$mysqli -> close();
					?>
						<form id="observe" action="?action=subjectView&subject=<?php echo($_GET["subject"]); ?>&observe=true" method="post">
							<p><input type="checkbox" value="true" name="observe" onclick="javascript: $('#observe').submit();" <?php echo($observed); ?>> Dieses Fach beobachten (E-Mail Benachrichtigung)</p>
						</form>
					<?php
				}
				//Select all homeworks and left joined the uploads that belong to it for this subject
				$query = $this->matse->db()->prepare("SELECT hu.ID, hu.Author, hw.ID, hw.Name, hw.Due, hw.Date, hw.file, hw.location, hu.Path, hu.Date AS Uploaddate, us.Name AS Username, SUM(rep.Score) AS Score FROM Homework hw LEFT JOIN UploadsHomework hu LEFT JOIN Users us ON us.ID = hu.Author ON hw.ID = hu.Homework LEFT JOIN Reputations rep ON rep.Item = hu.ID AND rep.ItemType = 1 WHERE hw.Subject = ? GROUP BY hu.ID, hw.ID ORDER BY hw.Due");
				echo($this->matse->db() -> error);
				$query->bind_param("i", $_GET["subject"]);
				$query -> execute();
				$query->bind_result($hid, $userid, $id, $name, $due, $date, $file, $location, $path, $uploaddate, $username, $score);
				$data = array();
				while($query->fetch())
				{
					if(empty($data[$name])) //Store all data in an associative array that can be read from later on
					{
						$data[$name] = array();
						$data[$name]["id"] = $id;
						$data[$name]["due"] = $due;
						$data[$name]["date"] = $date;
						$data[$name]["file"] = $file;
						$data[$name]["location"] = $location;
						$data[$name]["uploads"] = array();
					}
					if(!empty($path) && !empty($date))
						array_push($data[$name]["uploads"], array(
								"HID" => $hid, 
								"path" => $path, 
								"date" => $uploaddate, 
								"username" => $username, 
								"userid" => $userid, 
								"score" => $score)); //push all uploads into the array
				}
				$query -> close();
				foreach($data as $name => $values) //Walk through each found homework and display it's attributes and uploads
				{
					if(isset($_GET["locationAll"]) || $values["location"] == $this->matse->user->location){
						switch($values["location"]){
							case 0:
								$values["location"] = "Aachen";
								break;
							case 1:
								$values["location"] = "Jülich";
								break;
							case 2:
								$values["location"] = "Köln";
								break;
						}
						?>
						<div class="homework" id="hw_<?php echo($values["id"]); ?>">
							<div class="subject">
							<?php 
								echo(date("d.m.Y", $values["date"])." - ");
								if(time() < $values["due"]){
									echo("<span style=\"color:#EE7777\">");
								}
								else{
									echo("<span style=\"color:gray\">");
								}
								echo(date("d.m.Y", $values["due"])); ?>
								</span>
							</div>
							<a href="javascript: toggle('hw_<?php echo($values["id"]); ?>')"><h2><?php echo($values["location"]." - ".$name);?></h2></a>
								<?php 
									foreach($values["uploads"] as $upload) //Walk through each upload that belongs to this homework and display it
									{
										$username = $upload["username"];
										if($username == "") $username = "Anonymous"; //If the username is NULL, there was no user assigned to this ID and this was an anonymous upload
										$date = date("d.m.Y H:i:s", $upload["date"]); //Generate a human-readable date/time from the timestamp
										?>
											<a class="vote" href="?action=subjectView&subject=<?php echo( $_GET["subject"]); ?>&voteUpload=<?php echo($upload["HID"]); ?>&score=1">
												<div class="do">
													+1
												</div>
											</a>
											<a class="vote" href="?action=subjectView&subject=<?php echo( $_GET["subject"]); ?>&voteUpload=<?php echo($upload["HID"]); ?>&score=0">
												<div class="do">
													-1
												</div>
											</a>
										<?php
										if($userid == $this -> matse -> user -> userID || $this -> matse -> user -> hasPrivileg("DeleteUpload"))
										{
											?>
												<a class="delete" href="?action=subjectView&subject=<?php echo( $_GET["subject"]); ?>&deleteUpload=<?php echo($upload["HID"]); ?>">
													<div class="do">
														Delete
													</div>
												</a>
											<?php
										}
										if(empty($upload["score"])) $upload["score"] = 0;
										if($upload["score"] <= 0) $scorecolor = "red";
										else $scorecolor = "green";
										?>
											<a class="upload" href="<?php echo($upload["path"]); ?>" target="_blank">
												<div class="author">
													<?php echo($username); ?>
												</div>
												<div class="download">
													Download
												</div>
												<div class="author" style="color: <?php echo($scorecolor);?>;">
													<?php echo($upload["score"]); ?>
												</div>
												<div class="date">
													<?php echo($date); ?>
												</div>
											</a>
										<?php
									}
								?>	
							<br>
								<div class="new">
									<?php 
										if(empty($values["file"]))
										{
											?>
												<p>Kein Aufgabenblatt verfügbar</p>
											<?php 
										}
										else
										{
											?>
												<p><a href="<?php echo $values["file"];?>">Hier</a> kann das Aufgabenblatt heruntergeladen werden.</p>
											<?php 
										} 
										if($this->matse->user->loggedIn)
										{
											?>
												<p>Eine neue Lösung kann <a href="?action=uploadHomework&homework=<?php echo($values["id"]); ?>">hier</a> hochgeladen werden.</p>
											<?php
											if($this -> matse -> user -> hasPrivileg("DeleteHomework"))
											{
												?>
													<p>Sie können die Hausaufgabe <a href="?action=subjectView&subject=<?php echo( $_GET["subject"]); ?>&deleteHomework=<?php echo($values["id"]); ?>">hier</a> löschen.</p>
												<?php
											} 
										}
									?>
								</div>
						</div>
					<?php
					}
				}
				if($this->matse->user->hasPrivileg("CreateHomework")) //Display the link for creating new homework if the user has this privileg
				{
					?>
						<br>
						<p><a href="?action=homeworkCreate&subject=<?php echo($_GET["subject"]); ?>">Hier</a> kann eine neue Hausaufgabe hinzugefügt werden.</p>
					<?php
				}
				if($this->matse->user->hasPrivileg("ArchiveSemester")) //Display the link for archiving the current material if the user has this privileg
				{
					?>
						<br>
						<p><a href="?action=archive&subject=<?php echo($_GET["subject"]); ?>">Hausaufgaben archivieren</a>
					<?php
				}
			}
		}
	}
?>
