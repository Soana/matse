<?php 
	class ContentForum extends Content
	{

		public function printH1()
		{
			echo("Fragen &amp; Antworten");
		}
		
		private function deleteTopic()
		{
			$tid = $_GET["deleteTopic"];
			$mysqli = $this -> matse -> db() -> prepare("SELECT User FROM Questions WHERE User = ?");
			$mysqli -> bind_param("i", $tid);
			$mysqli -> execute();
			$mysqli -> bind_result($userID);
			$mysqli -> fetch();
			$mysqli -> close();
			if($this -> matse -> user -> hasPrivileg("DeleteQuestions"))
			{
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Questions WHERE ID = ?");
				$mysqli -> bind_param("i", $tid);
				$mysqli -> execute();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Answers WHERE Topic = ?");
				$mysqli -> bind_param("i", $tid);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Die Frage und alle zugehörigen Antworten wurden gelöscht.");
			}
			else
			{
				displayError("Sie haben nicht die Berechtigung, diese Frage zu löschen.");
			}
		}
		
		private function createTopic()
		{
			$topic = $_POST["topic"];
			$text = parse($_POST["content"]);
			$subject = $_POST["subject"];
			echo $subject;
			$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM Questions WHERE Topic LIKE ? AND Subject = ?");
			$mysqli -> bind_param("si", $topic, $subject);
			$mysqli -> execute();
			$exists = $mysqli -> fetch();
			$mysqli -> close();
			if($exists)
				displayError("Eine Frage mit exakt dem gleichen Titel in diesem Fach existiert schon. Bitte wähle einen aussagekräftigen Titel!");
			else
			{
				if(strlen($text) < 10)
					displayError("Der Text ist viel zu kurz, bitte gib mindestens 10 Zeichen ein. ");
				else
				{
					$mysqli = $this -> matse -> db() -> prepare("INSERT INTO Questions (Topic, Message, Date, User, Resolved, Subject) VALUES (?, ?, ?, ?, 0, ?)");
					$now = time();
					$mysqli -> bind_param("ssiii", $topic, $text, $now, $this -> matse-> user-> userID, $subject);
					$mysqli -> execute();
					$mysqli -> close();
					displaySuccess("Deine Frage wurde erfolgreich gestellt.");
				}
			}
		}
		
		public function printHTML()
		{
			if(!$this ->matse->user->loggedIn)
			{
				displayWarning("Das Forum steht nur registrierten Benutzern zur Verfügung, Sie können daher keine neuen Fragen stellen.");
			}
			else
			{
				if(isset($_GET["deleteTopic"]))
				{
					$this -> deleteTopic();
				}
				if(isset($_POST["topic"]))
				{
					$this -> createTopic();
				}
				?>
					<div class="homework" id="create">
						<a href="javascript: toggle('create')"><h2>Frage stellen</h2></a>
						<form action="?action=forum" method="post" style="margin-left: 15px; margin-bottom: 15px">
							<label>Topic</label>
							<input name="topic" type="text" style="width: 600px"/>
							<label>Text</label>
							<textarea id="msg" name="content" style="width: 600px; height: 400px">Befehle:&#10;[latex] Latex-Code Hier [/latex]&#10;[code=lang] Sourcecode hier [/code]&#10;Verfügbare Sprachen: c++, java, js, python, php, sql, c#</textarea>
							<label>Fach</label>
								<select name="subject" size="1">
									<option value="0">Kein bestimmtes Fach</option>
									<?php 
										$mysqli = $this -> matse -> db() -> prepare("SELECT ID, Name FROM Subjects");
										$mysqli -> execute();
										$mysqli -> bind_result($id, $name);
										while($mysqli -> fetch())
										{
											?>
												<option value="<?php echo($id); ?>"><?php echo($name);?></option>
											<?php
										}
										$mysqli -> close();
									?>
								</select>
							<label>Frage stellen</label>
							<input type="submit" value="OK" />
							<p>Bitt gib, wenn möglich das Fach an, zu dem diese Frage gehört. Dadurch ist es anderen Benutzern möglich, durch ihre Antwort in diesem Fach zu Reputation zu kommen.</p>
						</form>
					</div>
				<?php 
			}
			?>
				<h2>Fragen</h2>
			<?php
			$size = 20;
			if(isset($_GET["num"]))
			{
				$page = $_GET["num"];
			}
			else 
				$page = 1;
			$upper = $page * $size - $size;
			$mysqli = $this -> matse -> db() -> prepare("SELECT ID FROM Questions");
			$mysqli -> execute();
			$mysqli -> store_result();
			$amnt = $mysqli -> num_rows;
			$mysqli -> close();
			$pages = ceil(($amnt / $size + 0.0));
			$mysqli = $this -> matse -> db() -> prepare("SELECT q.ID, q.Date, q.Topic,  q.User, q.Message, q.Subject, q.Resolved, u.Name, s.Name FROM Questions q Left JOIN Subjects s ON q.Subject = s.ID LEFT JOIN Users u ON u.ID = q.User ORDER BY q.Resolved ASC, q.Date DESC LIMIT ?, ?");
			$mysqli -> bind_param("ii", $upper, $size);
			$mysqli -> execute();
			$mysqli -> bind_result($id, $dateRaw, $topic,  $userId, $txt, $subId, $resolved, $userName, $subjectName);
			while($mysqli->fetch())
			{
				$date = date("d.m.Y H:i", $dateRaw);
				if(empty($subjectName)) $subjectName = "Kein Fach";
				if(empty($userName)) $userName = "Kein Benutzer";
				?>
					<div class="homework" id="topic_<?php echo($id);?>">
						<div class="subject">
							<?php echo($subjectName); ?>
						</div>
						<div class="info">
							<?php echo($date); ?> von <?php echo($userName." "); ?>
						</div>
						<?php 
							
							if(!$resolved)
							{
								?>
									<div class="subject">
										<span style="color: red; font-style: normal;">Ungelöst</span>
									</div>
								<?php
							}	
						?>
						<a href="javascript: toggle('topic_<?php echo($id);?>')"><h2><?php echo($topic); ?></h2></a>
						<?php 
							if($resolved)
							{
								?>
									<p>Diese Frage wurde bereits beantwortet</p>
								<?php
							}
							else
							{
								?>
									<p>Diese Frage wurde noch nicht richtig beantwortet</p>
								<?php
							}
						?>
						<p>Der ganze Topic kann <a href="?action=viewTopic&id=<?php echo($id);?>">hier</a> betrachtet werden.</p>
						<div class="text">
							<?php echo($txt);?>
						</div>
					</div>
				<?php
			}
			$mysqli -> close();
			?>
				<div class="chooser">
					<?php 
						for($i = 1; $i <= $pages; $i++)
						{
							if($i == $page)
							{
								?>
									<a class="selected" href="?action=forum&num=<?php echo($i);?>"><?php echo($i); ?></a>
								<?php
							}
							else
							{
								?>
									<a class="unselected" href="?action=forum&num=<?php echo($i);?>"><?php echo($i); ?></a>
								<?php
							}
						}
					?>
				</div>
				<script type="text/javascript">
					MathJax.Hub.Queue(["Typeset",MathJax.Hub,"messages"]);
					SyntaxHighlighter.highlight();
					var init = false;
					$('#msg').click(function()
					{
						if(!init)
						{
							$('#msg').css("color", "black");
							$('#msg').css("font-style", "normal");
							$('#msg').val("");
							init = true;
						}
					});
				</script>
			<?php
			
		}
	}
?>
