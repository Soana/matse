<?php
	/*
	 * Allows the user to login
	 */
	class ContentLogin extends Content
	{
		public function printH1()
		{
			echo("Anmelden");
		}
		
		private function login()
		{
			$nickname = $_POST["nickname"]; //Grep data from array for faster access
			$password = $_POST["password"];
			$password = sha1($password); //Hash the password to crosscheck it in the database
			$query = $this->matse->db()->prepare("SELECT ID FROM Users WHERE Name = ? AND Password = ?");
			$query -> bind_param("ss", $nickname, $password);
			$query -> execute();
			$query -> bind_result($id);
			if($query -> fetch())//If there is at least one pair of data with the supplied data => The login was valid
			{
				displaySuccess("Sie wurden erfolgreich angemeldet. In 1 Sekunde geht es weiter. Viel Vergnügen.");
				?>
					<script type="text/javascript">
							setTimeout(function(){location.replace("index.php?action=start")}, 1000);
					</script>
				<?php 
				$_SESSION["userid"] = $id;//Set the sessionarguments to log the user in
				$_SESSION["password"] = $password;
			}
			else
				displayError("Die angegebene Kombination aus Benutzername und Passwort sind nicht korrekt.");
		}
		/*
		 * Render page
		 */
		public function printHTML()
		{
			if(isset($_POST["nickname"])) //The user has sent the filled form if this argument is set
			{
				$this -> login();
			}
			?>
				<form action="#" method="POST">
					<p>
						<label>Nickname</label>
						<input name="nickname" type="text"/>
					</p>
					<p>
						<label>Passwort</label>
						<input name="password" type="password"/>
					</p>
					<p>
						<label>Speichern</label>
						<input id="submit" type="submit" value="Okay"/>
					</p>
				</form>
			<?php
		}
	}
?>