<?php 

	class ContentViewUser extends Content
	{
		public function printH1()
		{
			if(isset($_GET["id"]))
				$name = $this -> matse -> getUsername($_GET["id"]);
			else if(isset($_GET["name"]))
				$name = $_GET["name"];
			else $name = "Unknown";
			?>
				<?php echo($name); ?>
			<?php
		}
		
		private function changeMail($id)
		{
			if($id == $this -> matse -> user -> userID)
			{
				$mysqli = $this -> matse -> db() -> prepare("UPDATE Users SET Mail = ? WHERE ID = ?");
				$mysqli -> bind_param("si", $_POST["mail"], $id);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("E-Mail-Adresse erfolgreich in \"" . $_POST["mail"] . "\" geändert!");
			}
			else
				displayError("Sie haben nicht die Berechtigung, dies zu tun!");
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if(!isset($_GET["id"]) && !isset($_GET["name"]))
				displayError("Ein erwarteter Parameter wurde nicht übergeben");
			else
			{
				$id = isset($_GET["id"]);
				if($id)
				{
					$uid = $_GET["id"];
					$name = $this -> matse -> getUsername($uid);
				}
				else
				{
					$name = $_GET["name"];
					$query = $this->matse->db()->prepare("SELECT ID FROM Users WHERE Name=?");
					$query->bind_param("s",$name);
					$query->execute();
					$query->bind_result($uid);
					$query->close();
				}
				if(isset($_GET["changeMail"])) $this -> changeMail($uid);
				$mysqli = $this -> matse -> db() -> prepare("SELECT SUM(r.Score) AS Score, s.Name as Subject FROM Reputations r LEFT JOIN Subjects s ON s.ID = r.Subject WHERE r.User = ? GROUP BY r.Subject");
				$mysqli -> bind_param("i", $uid);
				$mysqli -> execute();
				$mysqli -> bind_result($score, $subject);
				?>
					<h2>Reputation</h2>
					<table>
						<tr class="head">
							<td>Fach</td>
							<td>Reputation</td>
						</tr>
						<?php
							while($mysqli -> fetch())
							{
								if(empty($subject)) $subject = "Generell";
								if($score <= 0) $scorecolor = "red";
								else $scorecolor = "green";
								?>
									<tr>
										<td><?php echo($subject); ?></td>
										<td style="color: <?php echo($scorecolor); ?>"><?php echo($score);?></td>
									</tr>
								<?php 
							}
						?>
					</table>
					<p>Hinweis: Ist ein Fach nicht angegeben, so hat niemals Aktivität in diesem Fach durch diesen Benutzer stattgefunden</p>
				<?php
				$mysqli -> close();
				if($uid == $this -> matse -> user -> userID || $this -> matse -> user -> hasPrivileg("DeleteUser"))
				{
					?>
						<h2>Löschen</h2>
					<?php
					if(isset($_GET["delete"]))
					{
						$del = $_GET["delete"];
						if($del == 1)
						{
							?>
								Sind Sie sicher? <a href="?action=viewUser&id=<?php echo($uid); ?>&delete=2">Ja, unwiederruflich löschen</a>.
							<?php
						} 
						if($del == 2)
						{
							$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Users WHERE ID = ?");
							$mysqli -> bind_param("i", $uid);
							$mysqli -> execute();
							$mysqli -> close();
							displaySuccess("Benutzer gelöscht");
						}
					}
					else
					{
						?>
							Benutzer permanent <a href="?action=viewUser&id=<?php echo($uid); ?>&delete=1">löschen</a>.
						<?php
					}
					?>
						<br>
						<br>
					<?php	
				}
				if($this -> matse -> user -> userID == $uid)
				{
					?>
					<h2>E-Mail-Adresse ändern</h2>
					<form action="?action=viewUser&id=<?php echo($uid); ?>&changeMail=true" method="post">
						<label>Neue Adresse:</label>
						<input type="text" name="mail" />
						<label>Speichern</label>
						<input type="submit" value="OK" />
					</form>
					<br>
					<br>
					<?php 
				}
				if($this -> matse -> user -> hasPrivileg("RenameUser"))
				{
					?>
						<h2>Umbenennen</h2>
						<form action="?action=viewUser&id=<?php echo($uid); ?>&rename=true" method="post">
							<input type="text" name="name" /><input type="submit" value="OK"/>
						</form>
					<?php
					if(isset($_GET["rename"]) && $_GET["rename"] == "true" && isset($_POST["name"]))
					{
						$mysqli = $this -> matse -> db() -> prepare("UPDATE Users SET NAME = ? WHERE ID = ?");
						$mysqli -> bind_param("si", $_POST["name"], $uid);
						$mysqli -> execute();
						$mysqli -> close(); 
						displaySuccess("Benutezr erfolgreich umbenannt");
					}
				}
			}
		}
	}

?>
