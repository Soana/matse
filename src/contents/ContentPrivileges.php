<?php
	/*
	 * Allows an user to modify the privileges of other users
	 */
	class ContentPrivileges extends Content
	{

		public function printH1()
		{
			echo("Privilegien verwalten");
		}
		
		private function changed()
		{
			$query = $this->matse->db()->prepare("DELETE FROM Privileges WHERE User = ?");//Delete all old privileges
			$query -> bind_param("i", $_GET["user"]);
			$query -> execute();
			$query -> close();
			//The user does not have any privileges by now
			foreach($this->matse->privileges as $privileg) //Now reenter every single privilege manually if it was checked
			{
				if(isset($_POST[$privileg->id]) && $_POST[$privileg->id] == "true")
				{
					$query = $this->matse->db()->prepare("INSERT INTO Privileges (User, Privileg) VALUES (?, ?)"); //Add privileg
					$query -> bind_param("ii", $_GET["user"], $privileg->id);
					$query -> execute();
					$query -> close();
				}
			}
			displaySuccess("Die Rechte wurden erfolgreich neu gesetzt.");
			
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if(!$this -> matse->user->hasPrivileg("ManagePrivileges"))//If this user is not allowed to manage privileges...
			{
				?>
					<div class="error">
						<h1>Fehler</h1>
						Unberechtigter Zugriff!
					</div>
				<?php 
				return;//... Kick him out displaying an errorpage, also doesnt allow him to execute anything on base of his posted data
			}
			if(isset($_GET["user"]))//This needs to be set, otherwise we don't know which user to modify
			{
				$name = $this->matse->getUsername($_GET["user"]);
				if(isset($_POST["changed"])) //If postdata was received, modify the privileges on base of it
				{
					$this -> changed();
				}
				$query = $this->matse->db()->prepare("SELECT Privileg FROM Privileges WHERE User = ?"); //Grep all privileges the user currently has to prefill the form
				$query -> bind_param("i", $_GET["user"]);
				$query -> execute();
				$query -> bind_result($priv);
				$privs = Array();
				while($query -> fetch())
				{
					$privs[$priv] = true;
				}
				$query -> close();
				?>
					
					<p>Es wird der Benutzer "<?php echo($name); ?>" mit der ID <?php echo($_GET["user"]); ?> betrachtet</p>
					<form action="" method="POST">
						<input type="hidden" name="changed" value="true" />
						<table>
							<tr class="head">
								<td>Status</td>
								<td>Privileg</td>
							</tr>
							<?php
								foreach($this->matse->privileges as $privileg) //Display a checkbox for any possible privileg
								{
									?>
										<tr>
											<td>
												<input type="checkbox" name="<?php echo($privileg->id); ?>" 
													<?php 
														if(isset($privs[$privileg->id])) echo('checked'); //check the checkbox has the user currently has this privileg
													?>
												value="true"/>
											</td>
											<td><?php echo($privileg->caption); ?></td>
										</tr>
									<?php 
								}
							?>
						</table>
						<br />
						<label>Speichern</label><input type="submit" value="Okay">
					</form>
				<?php
			}
			else
			{
				?>
					<div class="error">
						<h1>Fehler</h1>
						Ein erwarteter Parameter wurde nicht übergeben!
					</div>
				<?php 
			}
		}
	}
?>