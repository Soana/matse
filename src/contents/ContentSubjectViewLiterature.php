<?php
	/*
	 * This Content displays a particular subject and all homeworks an uploads that belong to it
	 */
	class ContentSubjectViewLiterature extends Content
	{
		public function printH1()
		{
			echo("Literatur");
		}
		
		private function delete()
		{
			$mysqli = $this -> matse -> db() -> prepare("SELECT Author, Path FROM Literature WHERE ID = ?");
			$mysqli -> bind_param("i", $_GET["deleteUpload"]);
			$mysqli -> execute();
			$mysqli -> bind_result($userid, $path);
			$mysqli -> fetch();
			$mysqli -> close();
			if($userid == $this -> matse -> user -> userID || $this -> matse -> user -> hasPrivileg("DeleteUpload"))
			{
				@unlink($path);
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Literature WHERE ID = ?");
				$mysqli -> bind_param("i", $_GET["deleteUpload"]);
				$mysqli -> execute();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Reputations WHERE Item = ? AND ItemType = 2");
				$mysqli -> bind_param("i", $_GET["deleteUpload"]);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Der Upload wurde erfolgreich gelöscht.");
			}
			else
				displayError("Sie haben keine Berechtigung, diesen Upload zu löschen.");
		}
		
		private function vote()
		{
			if($this -> matse -> user -> loggedIn)
			{
				$mysqli = $this -> matse -> db() -> prepare("DELETE FROM Reputations WHERE Voter = ? AND Item = ? AND ItemType = 2");
				$mysqli -> bind_param("ii", $this -> matse -> user -> userID, $_GET["voteUpload"]);
				$mysqli -> execute();
				$mysqli -> close();
				if($_GET["score"] == "1") $score = 1;
				else $score = -1;
				$mysqli = $this -> matse -> db() -> prepare("SELECT Author, Subject FROM Literature WHERE ID = ?");
				$mysqli -> bind_param("i", $_GET["voteUpload"]);
				$mysqli -> execute();
				$mysqli -> bind_result($user, $subject);
				$mysqli -> fetch();
				$mysqli -> close();
				$mysqli = $this -> matse -> db() -> prepare("INSERT INTO Reputations (User, Voter, Item, ItemType, Subject, Score) VALUES (?, ?, ?, 2, ?, ?)");
				$mysqli -> bind_param("iiiii", $user, $this -> matse -> user -> userID, $_GET["voteUpload"], $subject, $score);
				$mysqli -> execute();
				$mysqli -> close();
				displaySuccess("Upload wurde erfolgreich mit $score bewertet.");
			}
			else displaySuccess("Sie müssen angemeldet sein, um dies zu tun.");
		}
		
		/*
		 * Render the page
		 */
		public function printHTML()
		{
			if(isset($_GET["deleteUpload"]))
			{
				$this -> delete();
			}
			if(isset($_GET["voteUpload"]) && isset($_GET["score"]))
			{
				$this -> vote();
			}
			if(!isset($_GET["subject"])) //If there is no subject to display we don't know what to display and exit
			{
				displayError("Ein erwarteter Parameter wurde nicht übergeben!");
			}
			else
			{	
				//Select all literature that belongs to it for this subject
				$query = $this->matse->db()->prepare("SELECT l.ID, l.Name, l.Date, l.Path, l.Type, us.Name AS Username, us.ID, SUM(r.Score) FROM Literature l LEFT JOIN Users us ON us.ID = l.Author LEFT JOIN Reputations r ON r.Item = l.ID AND r.ItemType = 2 WHERE l.Subject = ? GROUP BY l.ID");
				$query->bind_param("i", $_GET["subject"]);
				$query -> execute();
				$query->bind_result($id, $name, $date, $path, $type, $username, $userid, $score);
				$data = array();
				while($query->fetch())
				{
					if(empty($data[$type])) //Store all data in an associative array that can be read from later on
					{	
						$data[$type] = array();
					}
					array_push($data[$type], array(
						"ID" => $id, 
						"Name" => $name, 
						"Date" => $date, 
						"Type" => $type,
						"path" => $path,
						"User" => $username,
						"UserID" => $userid,
						"Score" => $score)); 
				}
				//uasort($data, array("ContentSubjectViewLiterature", "sortScript"));
				foreach($data as $key => $values) //Walk through each found literature and display it's attributes and uploads
				{
					if($key == 1) $cat = "Zusammenfassungen";
					else if($key == 2) $cat = "Skripte";
					else if($key == 3) $cat = "Übungen";
					else if($key == 4) $cat = "Lösungen";
					else if($key == 5) $cat = "Sonstiges";
					else continue;
					?>
						<div class="homework" id="l_<?php echo($key); ?>">
						<a href="javascript: toggle('l_<?php echo($key); ?>')"><h2><?php echo($cat);?></h2></a>
							<?php 
								foreach($values as $data)
								{
									$username = $data["User"];
									if($username == "") $username = "Anonymous"; //If the username is NULL, there was no user assigned to this ID and this was an anonymous upload
									$date = date("d.m.Y H:i:s", $data["Date"]); //Generate a human-readable date/time from the timestamp
									?>
										<a class="vote" href="?action=subjectViewLiterature&subject=<?php echo( $_GET["subject"]); ?>&voteUpload=<?php echo($data["ID"]); ?>&score=1">
											<div class="do">
												+1
											</div>
										</a>
										<a class="vote" href="?action=subjectViewLiterature&subject=<?php echo( $_GET["subject"]); ?>&voteUpload=<?php echo($data["ID"]); ?>&score=0">
											<div class="do">
												-1
											</div>
										</a>
										<?php
											if($data["UserID"] == $this -> matse -> user -> userID || $this -> matse -> user -> hasPrivileg("DeleteUpload"))
											{
												?>
													<a class="delete" href="?action=subjectViewLiterature&subject=<?php echo( $_GET["subject"]); ?>&deleteUpload=<?php echo($data["ID"]); ?>">
														<div class="do">
															Delete
														</div>
													</a>
												<?php
											}
											if(empty($data["Score"])) $data["Score"] = 0;
											if($data["Score"] <= 0) $scorecolor = "red";
											else $scorecolor = "green";
										?>
										<a class="upload" href="<?php echo($data["path"]); ?>" target="_blank">
											<div class="author">
												<?php echo($username); ?>
											</div>
											<div class="download">
												<?php echo($data["Name"]); ?>
											</div>
											<div class="author" style="color: <?php echo($scorecolor);?>;">
													<?php echo($data["Score"]); ?>
											</div>
											<div class="date">
												<?php echo($date); ?>
											</div>
										</a>
									<?php	
								}
							?>
						</div>
					<?php
				}
				if($this -> matse -> user -> loggedIn)
				{
					?>
					<div class="new">
						Eine neue Zusammenfassung oder ein neues Skript kann <a href="?action=uploadLiterature&subject=<?php echo($_GET["subject"]); ?>">hier</a> hochgeladen werden.
					</div>
					<?php
				}
			}
		}
	}
?>