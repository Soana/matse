<?php
	class User
	{
		private $password; //SHA1-hashed password
		public $username; //Username
		public $loggedIn; //whether the user is logged in or not
		public $privileges; //an array containing all privileges of this user
		private $matse; //Pointer to main class
		public $userID; //Internal database ID of this user
		public $location; //location of the user
		public $mail; //e-mailadress
		
		/*
		 * Create a new instance of the user, store the pointer to the main class and attempt an login
		 */
		public function __construct($matse)
		{
			$this->privileges = array(); //Setup all possible privileges
			$this->matse = $matse; //Store the pointer to the main class
			if(isset($_SESSION) && isset($_SESSION["userid"]) && isset($_SESSION["password"])) //If there is a session and both username and password are set
			{
				$this->userID = $_SESSION["userid"]; //Retrieve logindata from sessionstorage
				$this->password = $_SESSION["password"];
				if(!empty($this->userID) && !empty($this->password)) //If the retrieved logindata was not empty...
				{
					$this->checkValidLogin(); //Check whether they are valid data and not manipulated in some way
					if($this->loggedIn) //If the check was successfull we are now logged in and will fetch the privileges for this user
					{
						$query = $this->matse->db()->prepare("SELECT Privileg FROM Privileges WHERE User = ?"); //Select all privileges for this user from the database
						$query -> bind_param("i", $this->userID);
						$query -> execute();
						$query->bind_result($priv);
						while($query->fetch())
						{
							array_push($this->privileges, $priv); //And push it to the array with the given privileges
						}
						$query->close();

						$query = $this->matse->db()->prepare("SELECT Location, Mail FROM Users WHERE ID = ?");
						$query->bind_param("i", $this->userID);
						$query->execute();
						$query->bind_result($this->location, $this->mail);
						$query->fetch();
						$query->close();
					}
				}
				else
					$this->loggedIn = false; //If the login was empty, we are not logged in
			}
			else
				$this->loggedIn = false; //If the session didn't exist, we are not logged in
		}
		
		/*
		 * Tests and returns whether this user has the specified privileg
		 */
		public function hasPrivileg($privileg)
		{
			return in_array($this->matse->getPrivileg($privileg)->id, $this->privileges); //Look, if the id is contained within the array of all our privileges
		}
		
		/*
		 * Checks, whether the supplied logindata is correct and modifies the local attributes to be a logged in user if so
		 */
		private function checkValidLogin()
		{
			$query = $this->matse->db()->prepare("SELECT Name FROM Users WHERE ID = ? AND Password = ?"); //fetch a dataset from the database that has both username an hashed password exactly matching
			$query->bind_param("is", $this->userID, $this->password);
			$query -> execute();
			$query -> bind_result($name);
			if($query -> fetch()) //If the query did not return an empty result there was at last one dataset matching, so the logindata is correct
			{
				$this->loggedIn = true;
				$this->username = $name;
			}
			else //Otherwise the logindata was wrong as the query returned an empty result
				$this->loggedIn = false;
			$query -> close();
		}
	}
?>