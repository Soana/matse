<?php
	class Config
	{
		private $config;
		
		/*
		 * Open and parse supplied configfile and store its data in the local attribute $config
		 */
		public function __construct($filename)
		{
			$fh = fopen($filename,"r");  //Open configfile
			while($buffer = fgets($fh))
			{
				$buffer = str_replace(" ", "", $buffer); //Clean whitespaces
				$buffer = str_replace("\n", "", $buffer); //Clean line breaks
				$buffer = str_replace("\r", "", $buffer); 
				$buffer = explode("=", $buffer);//Split
				$this->config[$buffer[0]] = $buffer[1]; //Save configuration
			}
			fclose($fh); //Close configfile
		}
		
		/*
		 * Return a value from the previously parsed data
		 */
		function get($key)
		{
				return $this->config[$key];
		}
	}
?>