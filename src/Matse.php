<?php
	/* 
	 * All really important includes
	 */
	require_once("src/Config.php");
	require_once("src/Navigation.php");
	require_once("src/Content.php");
	require_once("src/User.php");
	require_once("src/SMTPClient.php");
	require_once("src/Parse.php");
	require_once("src/Messages.php");
	/*
	 * Includes of classes for contents to display
	 */
	require_once("src/contents/ContentRegister.php");
	require_once("src/contents/ContentStart.php");
	require_once("src/contents/ContentLogin.php");
	require_once("src/contents/ContentLogout.php");
	require_once("src/contents/ContentUserlist.php");
	require_once("src/contents/ContentPrivileges.php");
	require_once("src/contents/Content404.php");
	require_once("src/contents/ContentSubjectCreate.php");
	require_once("src/contents/ContentSubjectlist.php");
	require_once("src/contents/ContentSubjectView.php");
	require_once("src/contents/ContentHomeworkCreate.php");
	require_once("src/contents/ContentUploadHomework.php");
	require_once("src/contents/ContentChat.php");
	require_once("src/contents/ContentAjaxMenu.php");
	require_once("src/contents/ContentForum.php");
	require_once("src/contents/ContentImpressum.php");
	require_once("src/contents/ContentViewUser.php");
	require_once("src/contents/ContentViewTopic.php");
	require_once("src/contents/ContentSubjectViewLiterature.php");
	require_once("src/contents/ContentUploadLiterature.php");
	require_once("src/contents/ContentArchive.php");
	
	/*
	 * The main class of this program
	 */
	class Matse
	{
		private $dba; //Object to communicate with the database, please use Matse::db() instead for statistics!
		public $navigation; //Navigation holding all pages
		public $querys; //Number of query executed by now
		public $user;  //Currently logged in user
		public $privileges; //All privileges known to this server
		public $smtpClient;
		
		/*
		 * Set up configuration, user and databaseconnection
		 */
		public function __construct()
		{
			session_start(); //Start the session holding the logininformation (possibly)
			$this->setupPrivileges(); //Setup all privileges this server should know about
			$this->navigation = new Navigation($this); //Create the navigation
			$this->dba = new mysqli($GLOBALS["databaseConfig"]["URL"],$GLOBALS["databaseConfig"]["User"],$GLOBALS["databaseConfig"]["Password"]); //And setup the databaseconnection
			$this->dba -> select_db($GLOBALS["databaseConfig"]["Database"]); //Select the database to read from
			echo($this->dba->error); //Echo any errors belonging to the databasesetup
			$this->checkAndInstallDatabase(); //If neccessary, generate all tables for the database
			$this->user = new User($this); //Create a new userinstance that will then try to login
			$this->setupContents(); //Setup all known contents to the navigation
			$this->smtpClient = new SMTPClient($GLOBALS["smtpConfig"]["Server"], $GLOBALS["smtpConfig"]["Port"], $GLOBALS["smtpConfig"]["User"], $GLOBALS["smtpConfig"]["Password"], "Matse-Boerse <matse@cronosx.de>"); //Create new SMTPClient to send mails from
		}
		
		/*
		 * Returns a pointer to the global mysql databaseconnection and handles statistics
		 */
		public function db()
		{
			$this -> querys++; //Increase the amount of querys executed on the database
			return $this -> dba;
		}
		
		/*
		 * Returns the privileg by its name
		 */
		public function getPrivileg($name)
		{
			return $this->privileges[$name];
		}
		
		/*
		 * Registers a new possible privileg
		 */
		public function addPrivileg($name, $caption, $i)
		{
			$this->privileges[$name] = new Privileg($name, $i, $caption);
		}
		
		/*
		 * Sets up all privileges known to this software
		 */
		private function setupPrivileges()
		{
			$i = 1;
			$this->addPrivileg("ManagePrivileges", "Privilegien verwalten", $i++);
			$this->addPrivileg("TestPrivileg", "Test Privileg", $i++);
			$this->addPrivileg("CreateSubject", "Fach hinzufügen", $i++);
			$this->addPrivileg("CreateHomework", "Hausaufgabe hinzufügen", $i++);
			$this->addPrivileg("DeleteQuestions", "Fragen anderer Leute löschen", $i++);
			$this->addPrivileg("CloseQuestions", "Fragen anderer Leute schliessen", $i++);
			$this->addPrivileg("DeleteAnswers", "Antworten anderer Leute löschen", $i++);
			$this->addPrivileg("DeleteUpload", "Uploads anderer Leute löschen", $i++);
			$this->addPrivileg("DeleteHomework", "Hausaufgaben löschen", $i++);
			$this->addPrivileg("DeleteUser", "Benutzer löschen", $i++);
			$this->addPrivileg("RenameUser", "Benutzer umbenennen", $i++);
			$this->addPrivileg("ArchiveSemester", "Material archivieren", $i++);
		}
		
		/*
		 * Sets up all contents known to this software
		 */
		private function setupContents()
		{
			$this->navigation->addEntry(new ContentStart($this), "start", "Startseite");
			$this->navigation->addEntry(new Content404($this), "404");
			$this->navigation->addEntry(new ContentUploadHomework($this), "uploadHomework");
			$this->navigation->addEntry(new ContentArchive($this), "archive");
			$this->navigation->addEntry(new ContentSubjectlist($this), "toggleSemesterMenu();", "Fächer", true);
			$this->navigation->addEntry(new ContentUploadLiterature($this), "uploadLiterature");
			$this->navigation->addEntry(new ContentSubjectView($this), "subjectView");
			$this->navigation->addEntry(new ContentAjaxMenu($this), "ajaxMenu");
			$this->navigation->addEntry(new ContentSubjectViewLiterature($this), "subjectViewLiterature");
			$this->navigation->addEntry(new ContentForum($this), "forum", "Forum");
			$this->navigation->addEntry(new ContentViewTopic($this), "viewTopic");
			$this->navigation->addEntry(new ContentViewUser($this), "viewUser");
			$this->navigation->addEntry(new ContentImpressum($this), "impressum");
			if($this->user->loggedIn)
			{
				$this->navigation->addEntry(new ContentUserlist($this), "userlist", "Benutzer");
				$this->navigation->addEntry(new ContentHomeworkCreate($this), "homeworkCreate");
				$this->navigation->addEntry(new ContentPrivileges($this), "privileges");
				$this->navigation->addEntry(new ContentSubject($this), "subjectCreate");
				$this->navigation->addEntry(new ContentChat($this), "chat", "Chat");
				$this->navigation->addEntry(new ContentLogout($this), "logout", "Abmelden");
			}
			else
			{
				$this->navigation->addEntry(new ContentRegister($this), "register", "Registrieren");
				$this->navigation->addEntry(new ContentLogin($this), "login", "Anmelden");
			}
		}
		
		/*
		 * Prints the HTML generated by this class
		 */
		public function printHTML()
		{
			if(!$_GET["silent"]) 
			{
				$this->navigation->printHTML();
				?>
					<div class="container">
					<div class="headline">
						<?php 
							$this->navigation->getContent()->printH1();
						?>
					</div>
						<div class="content">
							<?php
			}
			$this->navigation->getContent()->printHTML();
			if(!$_GET["silent"]) 
			{
							?>
						</div>
					</div>
				<?php
			}
		}
		
		/*
		 * Creates all necessary tables, if they are not created already
		 */
		private function checkAndInstallDatabase()
		{
			//Create a table containing all subjects (Analysis, Java, Algorithmen, ...)
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Subjects(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								Name			TEXT,
								Description		TEXT,
								Semester		INT)");
			//Create a table containing all homeworks
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Homework(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								Subject			INT,
								Name			TEXT,
								Due				INT,
								Date			INT,
								Location		INT,
								File			TEXT)");
			//Create a table containing all uploaded solutions for the homeworks
			$this->db() -> query("CREATE TABLE IF NOT EXISTS UploadsHomework(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								Homework		INT,
								Path			TEXT,
								Date			INT,
								Author			INT)");
			//Create a table containing all uploaded literatue (Scripts and Summarys)
			// 1 = Zusammenfassung, 2 = Skript, 3 = Übung, 4 = Lösung, 5 = Sonstiges
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Literature(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								Subject			INT,
								Type			INT,
								Path			TEXT,
								Date			INT,
								Author			INT,
								Name			TEXT)");
			//Create a table containing all users
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Users(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								Name			TEXT,
								Password		VARCHAR(40),
								Location		INT,
								Mail			TEXT,
								Online			BOOLEAN)");
			//Create a table containing all privileges assigned to all users
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Privileges(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								User			INT,
								Privileg		INT)");
			//Create a table containing reputation for each user in a certain subject
			// 1 = Homework, 2 = Literature, 3 = Answer
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Reputations(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								User			INT,
								Voter			INT,
								Item			INT,
								ItemType		INT,
								Subject			INT,
								Score			INT)");
			//Create a table containing reputation for each user in a certain subject
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Chat(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								User			INT,
								Message			TEXT,
								Date			INT)");
			//Create a table containing reputation for each user in a certain subject
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Questions(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								User			INT,
								Message			TEXT,
								Date			INT,
								Topic			TEXT,
								Subject			INT,
								Resolved		BOOLEAN)");
			//Create a table containing reputation for each user in a certain subject
			$this->db() -> query("CREATE TABLE IF NOT EXISTS Answers(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								User			INT,
								Message			TEXT,
								Date			INT,
								Topic			INT)");
			//Create a table to store all observed topics by user in
			$this->db() -> query("CREATE TABLE IF NOT EXISTS ObservedTopics(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								User			INT,
								Topic			INT)");
			//Create a table to store all observed subjects by user in
			$this->db() -> query("CREATE TABLE IF NOT EXISTS ObservedSubjects(
								ID				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
								User			INT,
								Subject			INT)");
		}
		
		/*
		 * Returns the username that belongs to the supplied database-ID
		 */
		public function getUsername($id)
		{
			$query = $this->db()->prepare("SELECT Name FROM Users WHERE ID = ?"); //Select the name to the given id from the database
			$query -> bind_param("i", $id);
			$query -> execute();
			$query -> bind_result($name);
			$query -> fetch();
			$query -> close();
			return $name;
		}
		
		/*
		 * Checks, whether a username is already taken or whether not
		 */
		public function usernameTaken($name)
		{
			$query = $this->db()->prepare("SELECT ID FROM Users WHERE Name = ?"); //Select the name to the given id from the database
			$query -> bind_param("s", $name);
			$query -> execute();
			if($query -> fetch()) 
			{
				$query -> close();
				return true;
			}
			else 
			{
				$query -> close();
				return false;
			}
		}
		
		/*
		 * Close the databaseconnection
		 */
		public function __destruct()
		{
			$this->dba->close();
		}
		
		/*
		 * Returns the name of a subject by its id
		 */
		public function getSubjectName($id)
		{
			$query = $this->db()->prepare("SELECT Name FROM Subjects WHERE ID = ?");  //Select the name to the given id from the database
			$query ->bind_param("i", $id);
			$query->execute();
			$query->bind_result($name);
			$query->fetch();
			$query->close();
			return $name;
		}

		/*
		 * Returns the name of a homework by its id
		*/
		public function getHomeworkName($id)
		{
			$query = $this->db()->prepare("SELECT Name FROM Homework WHERE ID = ?"); //Select the name to the given id from the database
			$query ->bind_param("i", $id);
			$query->execute();
			$query->bind_result($name);
			$query->fetch();
			$query->close();
			return $this->getCleanName($name);
		}

		public function getCleanName($name)
		{
			return str_replace(array("Ü","ü","Ö","ö","Ä","ä","ß", " "), array("Ue","ue","Oe","oe","Ae","ae","ss", "_"), $name);
		}
		/*
		 * Returns the name of a subject by the ID of a homework belonging to it 
		 */
		public function getSubjectNameByHomework($id)
		{
			
			$query = $this->db()->prepare("SELECT sub.Name FROM Subjects sub LEFT JOIN Homework hw ON hw.Subject = sub.ID WHERE hw.ID = ?");// left join the subject on the homework it is belonging to and return its name
			$query ->bind_param("i", $id);
			$query->execute();
			$query->bind_result($name);
			$query->fetch();
			$query->close();
			return $this -> getCleanName($name);
		}
		
	}
	
	/*
	 * Represents a privileg
	 */
	class Privileg
	{
		public $name; //Name of the privileg
		public $id; //internal id of the privileg
		public $caption; //Human readable name of the privileg
		
		/*
		 * Create a new privileg and store all attributes
		 */
		public function __construct($name, $id, $caption)
		{
			$this->name = $name;
			$this->id = $id;
			$this->caption = $caption;
		}
	}
?>
