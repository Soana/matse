<?php
	function parse($text)
	{
		$fin = "<p>";
		$rplBr = true;
		for($i = 0; $i < strlen($text); $i++)
		{
			$c = $text{$i};
			if($c == "\n" && $rplBr)
			{
				$fin .= "</p><p>";
			}
			else
			{
				if($c == '<')
				{
					$fin .= "&lt;";
				}
				else if($c == '"')
				{
					$fin .= "&quot;";
				}
				else if($c == '>')
				{
					$fin .= "&gt;";
				}
				else if($c == '[')
				{
					$i++;
					if($text{$i} == '/')
					{
						$i++;
						if(substr($text, $i, 5) == "code]")
						{
							$i += 5;
							$fin .= "</pre>";
							$rplBr = true;
						}
						else if(substr($text, $i, 6) == "latex]")
						{
							$i += 5;
							$fin .= "\)";
							$rplBr = true;
						}
						else
						{
							$fin .= $c;
							$i--;	
						}
					}
					else if(substr($text, $i, 4) == "code")
					{
						$i += 3;
						if($text{++$i} == '=')
						{
							$lang = "";
							while(($c = $text{++$i}) != ']') 
								$lang .= $c;
							if(		$lang != "python"
								&&  $lang != 'c++' 
								&&  $lang != "c#" 
								&&  $lang != "java" 
								&&  $lang != "js"
								&&  $lang != "php" 
								&&  $lang != "sql") $lang = "plain";
							$fin .= '<pre class="brush: '.$lang.';">';
							$rplBr = false;
						}
						else {
							$i -= 5;
							$fin .= $c;
						}
					}
					else if(substr($text, $i, 6) == 'latex]')
					{
						$fin .= "\(";
						$i += 5;
						$rplBr = false;
					}
					else 
					{
						$fin .= $c;
						$i--;
					}
				}
				else
					$fin .= $c;
			}
		}
		$fin .= "</p>";
		return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>',$fin);
	}
?>