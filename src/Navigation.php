<?php
	/*
	 * Does not only represent and create an HTML navigation for the page but also contains all contents registered to this server
	 */
	class Navigation
	{
		private $matse; //Pointer to the main class
		private $amount; //Amount of currently registered contents
		private $entrys; //All contents registered to this navigation
		
		/*
		 * Create a new instance of this navigation and store the pointer to the mainclass
		 */
		public function __construct($matse)
		{
			$this -> amount = 0;
			$this -> matse = $matse; //Save main pointer
		}
		
		/*
		 * Add a new content to this navigation
		 */
		public function addEntry($content, $name, $caption = "", $js = false)
		{
			$this -> entrys[$name] = new NavigationEntry($content, $caption, $name, $js); //Store the entry
			$this -> amount++; //Increase total amount of entrys
		}
		
		/*
		 * Renders the navigation
		 */
		public function printHTML()
		{
			?>
				<div class="container">
				<div class="header"></div>
				<div class="navigation">
					<ul>
						<?php 
							foreach($this ->entrys as $entry) //Echo each entry
							{
								if($entry->caption != "") //But only if it has a human readable name, otherwise it is not mapped and belongs silently to this navigation (Called from somewhere else)
								{
									if(!$entry->js) $action = "?action=".$entry->id;
									else $action="javascript: ".$entry->id;
									?>
										<li><a href="<?php echo($action);?>"><?php echo($entry->caption); ?></a></li>
									<?php
								}
							}
						?>
					</ul>
					<?php
						if($this->matse->user->loggedIn) //If the user is currently logged in, display an infobox to indicate that
						{
							?>
								<div class="userInfo">
									Angemeldet als: <a href="?action=viewUser&id=<?php echo($this -> matse -> user -> userID); ?>"><?php echo($this->matse->user->username);?></a>
								</div>
							<?php
						}
					?>
				</div>
				</div>
				<div class="navigationsmall" id="semester"></div>
				<div class="navigationsmall" id="subject"></div>
				<script type="text/javascript">
				var oldsem;
				var semm = $('#semester');
				var subm = $('#subject');
				<?php 
					if(!empty($_SESSION["sem"]) && $_SESSION["sem"]) 
					{
						?>
						oldsem = <?php echo($_SESSION["sem"]);?>;
						<?php
					}
				?>
				
				function toggleSemesterMenu()
				{
					if(semm.vis)
					{
						if(subm.vis) subm.show(function() {semm.show(); });
						else semm.show();
						semm.vis = false;
						subm.vis = false;
					}
					else
					{
						semm.show();
						semm.vis = true;
						semm.html('<div class="load" id="seml"><img src="style/ajax-loader-width.gif"/></div>');
						$.ajax(
						{
							url: '?action=ajaxMenu&silent=true'
						}).done(function(response)
						{
							semm.html(response);
							console.log(response);
							if(oldsem !== undefined) toggleSubjectMenu(oldsem);
						});
					}
				}
				function toggleSubjectMenu(sem)
				{
					if(oldsem !== undefined)  $('#sem'+oldsem).removeClass("visited");
					subm.vis = true;
					$('#sem'+sem).addClass("visited");
					subm.show();
					subm.html('<div class="load" id="subl"><img src="style/ajax-loader-width.gif"/></div>');
					$.ajax(
					{
						url: '?action=ajaxMenu&silent=true&sem='+sem
					}).done(function(response)
					{
						subm.html(response);
						console.log(response);
					});
					oldsem = sem;
				}
				semm.hide();
				subm.hide();
				</script>
			<?php
		}
		
		/*
		 * Returns the content called by the GET-Argument "action" or the 404 content if this content was unknown 
		 */
		public function getContent()
		{
			if(!isset($_GET["action"])) $_GET["action"] = "start";
			if(!isset($this->entrys[$_GET["action"]])) $_GET["action"] = "404"; //If content is unknown: return 404 content
			return $this->entrys[$_GET["action"]]->content; //Else return fetched content
		}
	}
		
	/*
	 * Stores all data known to an entry of the navigation
	 */
	class NavigationEntry
	{
		public $content;//Content associated with this id
		public $id; //Internal id
		public $caption; //Human readable name of the content
		public $js; //If clicking should execute JS instead of redirecting
		
		/*
		 * Create a new instance and store all its data
		 */
		public function __construct($content, $caption, $id, $js)
		{
			
			$this -> id = $id;
			$this -> content = $content;
			$this -> caption = $caption;
			$this -> js = $js;
		}
	}
?>